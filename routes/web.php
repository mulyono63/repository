<?php

//akses login
Route::get('/','FrontendController@index');
// Route::get('/','AuthController@login');
Route::get('/login','AuthController@login')->name('login');
Route::post('postlogin','AuthController@postlogin');
Route::get('/logout','AuthController@logout');

//Daftar
Route::get('/daftar','AuthController@daftar');
Route::post('/createaccount','AuthController@create');
Route::get('/lupa','AuthController@lupa');
Route::post('/pospassa','AuthController@pospass');
Route::post('/pospass_update','AuthController@pospass_update');
//Search
Route::get('/search','FrontendController@search');

//penelitian
Route::get('/dataout/{id}','FrontendController@searchdataout');
Route::get('/repositoryp','FrontendController@seearchrepositoryp');
Route::get('/searchp','FrontendController@searchp');
Route::get('/repositorys','FrontendController@seearchrepositorys');
Route::get('/searchs','FrontendController@searchs');
Route::get('/repositoryj','FrontendController@seearchrepositoryj');
Route::get('/searchj','FrontendController@searchj');
Route::get('/dosen/{pembimbing}','FrontendController@profile_dosen');
Route::get('/dosen_list','FrontendController@dosen_list');


Route::get('/home','controllerwelcome@home');
Route::POST('welcome/add_wel','controllerwelcome@add');
Route::get('alert','controllerwelcome@alert');


//Akses Admin
Route::group(['middleware'=>['auth','checkRole:admin']],function(){

});

//Akses Master
Route::group(['middleware'=>['auth','checkRole:admin,mahasiswa,dosen']],function(){
//statistik
Route::get('/statistik','MasterController@statistik');	

Route::get('/dosenin/{pembimbing}','FrontendController@profile_dosenin');
Route::get('/dosen_listin','FrontendController@dosen_listin');
Route::get('/dashboard','MasterController@index');
Route::get('/profile','Profile@profile');
Route::get('/datadocumen','Profile@documen');
Route::post('/posprofil','Profile@posprofil');
Route::post('/posep','Profile@posep');
Route::post('/pospass','Profile@pospass');
//penelitian
Route::get('/searchin','FrontendController@searchin');
Route::get('/search_in','FrontendController@search_in');
Route::get('/repositorypen','FrontendController@seearchrepositorypen');
Route::get('/searchpen','FrontendController@searchpen');
Route::get('/data/{id}','FrontendController@searchdata');
Route::get('/penelitian','MasterController@penelitian');
Route::get('/data_file/{id}','MasterController@data_file');
Route::post('/postpenelitian','MasterController@postpenelitian');
Route::get('/deletepenelitian/{id}/{file}','MasterController@deletepenelitian');
//skripsi
Route::get('/repositoryskr','FrontendController@seearchrepositoryskr');
Route::get('/searchskr','FrontendController@searchskr');
Route::post('/postskripsi','MasterController@postskripsi');
Route::get('/deleteskripsi/{id}/{file}','MasterController@deleteskripsi');
Route::get('/dosen','MasterController@dosen');
Route::get('/skripsi','MasterController@skripsi');
Route::post('/postdosen','MasterController@postdosen');
//jurnal
Route::get('/repositoryjur','FrontendController@seearchrepositoryjur');
Route::get('/searchjur','FrontendController@searchjur');
Route::get('/jurnal','MasterController@jurnal');
Route::post('/postjurnal','MasterController@postjurnal');
Route::get('/deletejurnal/{id}/{file}','MasterController@deletejurnal');

Route::get('/mahasiswa','MasterController@mahasiswa');
Route::get('/tag','MasterController@tag');
Route::post('/posttag','MasterController@posttag');
Route::get('/deletetag/{id}','MasterController@deletetag');
Route::post('/activdsn','MasterController@activdsn');
Route::post('/activmhs','MasterController@activmhs');
});

//Akses mahasiswa
Route::group(['middleware'=>['auth','checkRole:mahasiswa']],function(){

});

//Akses dosen
Route::group(['middleware'=>['auth','checkRole:dosen']],function(){
Route::post('/posdocumen','Profile@posdocumen');
Route::get('/deletedocumen/{id}/{file}','Profile@deletedocumen');
Route::post('/pospendidikan','Profile@pospendidikan');
Route::post('/posbiodata','Profile@posbiodata');
Route::get('/deletependidikan/{id}','Profile@deletependidikan');
});
