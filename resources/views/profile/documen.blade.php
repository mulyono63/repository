<div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 id="card-title">Daftar Data Skripsi</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="float-right">

<div style="padding-top: 50px;" class="col-md-12 col-sm-12 col-xs-12">
                     <div class="x_panel">
                        <div class="x_title">
                          <h2>Documen</h2>
                          <button type="button" class="btn btn-primary navbar-right" data-toggle="modal" data-target="#document" ><i class="fa fa-plus m-right-xs"></i>Tambah Documen</button>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                          <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Judul File</th>
                                <th>Nama File</th>
                                <th>Tanggal Posting</th>
                                <th>File</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php $no=0;
                              foreach($c as $c){
                              $no++; ?>
                              <tr>
                                <th scope="row">{{$no}}</th>
                                <td>{{$c->judul_file}}</td>
                                <td>{{$c->nama_file}}</td>
                                <td>{{$c->created_at}}</td>
                                <td><a href="{{asset('documen/'.$c->nama_file)}}"><img  src="{{asset('img/userimg/file.png')}}"></a></td>
                                <td><a href="deletedocumen/{{$c->id}}/{{$c->nama_file}}" class="btn btn-danger btn-sm">Hapus</a></td>
                              </tr>
                              <?php } ?>
                            </tbody>
                          </table>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   </div>
  </div>
  <div class="modal fade"  id="document" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      <form method="POST" action="posdocumen" class="form-horizontal" id="form-tambah"  enctype="multipart/form-data">
        {{csrf_field()}}
          <div class="form-group">
            <input type="text" class="form-control" name="judul_file" placeholder="Judul Document" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">File Document</label>
            <input type="file" class="form-control" name="nama_file" required>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
        </form>
    </div>
  </div>
</div>