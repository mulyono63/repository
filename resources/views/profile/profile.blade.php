<div class="right_col" role="main" style="min-height: 1264px;">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>User Profile</h3>
              </div>
            </div>
            @foreach ($errors->all() as $error)
                 <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
                      @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ $message }}</strong>
                            </div>
                        @elseif ($message = Session::get('gagal'))
                            <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>User Report <small>Activity report</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img data-toggle="modal" data-target="#exampleModal1" class="img-responsive avatar-view" src="{{asset('img/userimg/'.Auth::user()->foto)}}" alt="Avatar" title="Change the avatar">
                        </div>
                      </div>
                      <h3>{{Auth::user()->name}}</h3>

                      <ul class="list-unstyled user_data">
                        <li>
                          Email  {{Auth::user()->email}}</br>
                          NPM    {{Auth::user()->npm}}</br>
                        </li>
                      </ul>
                      <br>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Setting</a>
                          </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal4" ></i>Ganti Password</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- Profil awal -->
                  <?php if(Auth::user()->role == "dosen"){ ?>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Biodata Dosen</h2>
                          <?php
                              foreach($d as $a){
                             ?>
                          <button type="button" class="btn btn-success btn-sm navbar-right" data-toggle="modal" data-target="#biodata"  onclick="editbiodata(`{{$a->id}}`, `{{$a->nip}}`, `{{$a->nama}}`,`{{$a->gelar}}`,`{{$a->pendidikan}}`,`{{$a->agama}}`,`{{$a->jk}}`,`{{$a->nidn}}`,`{{$a->jabatan}}`,`{{$a->tempat_lahir}}`,`{{$a->tgl_lahir}}`)"><i class="fa fa-edit m-right-xs"></i>Edit Biodata</button>
                          <?php } ?>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                          <table class="table">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>NIDN</th>
                                <th>Dosen</th>
                                <th>Jabatan Fungsional</th>
                                <th>Tempat/Tgl Lahir</th>
                                <th>Jenis Kelamin</th>
                                <th>Pendidikan Tinggi</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php $no=0;
                              foreach($d as $a){
                              $no++; ?>
                              <tr>
                                <th scope="row">{{$no}}</th>
                                <td>{{$a->nidn}}</td>
                                <td>{{$a->nama}}, {{$a->gelar}}</td>
                                <td>{{$a->jabatan}}</td>
                                <td>{{$a->tempat_lahir}}, {{$a->tgl_lahir}}</td>
                                <td>{{$a->jk}}</td>
                                <td>{{$a->pendidikan}}</td>
                              </tr>
                              <?php } ?>
                            </tbody>
                          </table>

                        </div>
                      </div>
                    </div>

                    <div style="padding-top: 50px;" class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Riwayat Pendidikan</h2>
                          <button type="button" class="btn btn-primary navbar-right" data-toggle="modal" data-target="#rpendidikan" ><i class="fa fa-plus m-right-xs"></i>Tambah Riwayat Pendidikan</button>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                          <table class="table">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Perguruan Tinggi</th>
                                <th>Gelar Akademik</th>
                                <th>Tahun Ijazah</th>
                                <th>Jenjang</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php $no=0;
                              foreach($b as $a){
                              $no++; ?>
                              <tr>
                                <th scope="row">{{$no}}</th>
                                <td>{{$a->perguruan_tinggi}}</td>
                                <td>{{$a->gelar}}</td>
                                <td>{{$a->tahun_ijasah}}</td>
                                <td>{{$a->jenjang}}</td>
                                <td>
                                   <button type="button" class="btn btn-success btn-sm " data-toggle="modal" data-target="#rpendidikan"  onclick="editpendidikan(`{{$a->id}}`, `{{$a->id_dosen}}`, `{{$a->perguruan_tinggi}}`,`{{$a->gelar}}`,`{{$a->tahun_ijasah}}`,`{{$a->jenjang}}`)"><strong>Edit</strong></button>
                                    <a href="deletependidikan/{{$a->id}}" class="btn btn-danger btn-sm">Hapus</a>
                                </td>
                              </tr>
                              <?php } ?>
                            </tbody>
                          </table>

                        </div>
                      </div>
                    </div>


                    <div style="padding-top: 50px;" class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Telah Membimbing</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                          <table class="table">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Mahasiswa</th>
                                <th>Total Skripsi</th>
                                <th>Total Jurnal</th>
                                <th>Tottal Penelitian</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php $no=0;
                              foreach($f as $a){
                              $no++; ?>
                              <tr>
                                <th scope="row">{{$no}}</th>
                                <td>Mahasiswa</td>
                                <td>{{$a->total_skripsi}}</td>
                                <td>{{$a->total_jurnal}}</td>
                                <td>{{$a->total_penelitian}}</td>
                              </tr>
                              <?php } ?>
                            </tbody>
                          </table>

                        </div>
                      </div>
                    </div>



              <?php }elseif(Auth::user()->role == "admin"){ ?>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_content">

                          <table class="table">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>NIDN</th>
                                <th>Dosen</th>
                                <th>Jabatan Fungsional</th>
                                <th>Tempat/Tgl Lahir</th>
                                <th>Jenis Kelamin</th>
                                <th>Pendidikan Tinggi</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php $no=0;
                              foreach($d as $a){
                              $no++; ?>
                              <tr>
                                <th scope="row">{{$no}}</th>
                                <td>{{$a->nidn}}</td>
                                <td>{{$a->nama}}, {{$a->gelar}}</td>
                                <td>{{$a->jabatan}}</td>
                                <td>{{$a->tempat_lahir}}, {{$a->tgl_lahir}}</td>
                                <td>{{$a->jk}}</td>
                                <td>{{$a->pendidikan}}</td>
                              </tr>
                              <?php } ?>
                            </tbody>
                          </table>

                        </div>
                      </div>
                    </div>
              <?php } else { ?>
                <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Biodata Mahasiswa</h2>
                          <button type="button" class="btn btn-primary navbar-right" data-toggle="modal" data-target="#exampleModal2" ><i class="fa fa-edit m-right-xs"></i>Edit Biodata</button>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                          <table class="table">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>NPM</th>
                                <th>Nama Mahasiswa</th>
                                <th>Agama</th>
                                <th>Pendidikan</th>
                                <th>Tempat/Tgl Lahir</th>
                                <th>Jenis Kelamin</th>
                                <th>Email</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php $no=0;
                              foreach($d as $a){
                              $no++; ?>
                              <tr>
                                <th scope="row">{{$no}}</th>
                                <td>{{$a->a}}</td>
                                <td>{{$a->nama}}, {{$a->gelar}}</td>
                                <td>{{$a->agama}}</td>
                                <td>{{$a->pendidikan}}</td>
                                <td>{{$a->tempat_lahir}}, {{$a->tgl_lahir}}</td>
                                <td>{{$a->jk}}</td>
                                <td>{{Auth::user()->email}}</td>
                              </tr>
                              <?php } ?>
                            </tbody>
                          </table>

                        </div>
                      </div>
                    </div>
              <?php } ?>
                  <!-- Profil tutup -->
                </div>
              </div>
            </div>
          </div>
        </div>




<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      <form method="POST" action="posprofil" class="form-horizontal" enctype="multipart/form-data">
           {{csrf_field()}}
            <input type="hidden" class="form-control" name="id" value="{{Auth::user()->npm}}">
            <input type="hidden" class="form-control" name="foto_del" value="{{Auth::user()->foto}}">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Foto</label>
            <input type="file" class="form-control" name="foto">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
        </form>
    </div>
  </div>
</div>

<!-- Ganti Password -->
<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      <form method="POST" action="pospass_update" class="form-horizontal" enctype="multipart/form-data">
           {{csrf_field()}}
            <input type="hidden" class="form-control" name="email" value="{{Auth::user()->email}}">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">New Password</label>
            <input type="password" class="form-control" name="password">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Confirmatio New Password</label>
            <input type="password" class="form-control" name="confirmation">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
        </form>
    </div>
  </div>
</div>

<!-- //edit profile -->
<?php if(Auth::user()->role == "admin"){ ?>

<?php }elseif(Auth::user()->role == "dosen"){ ?>
<div class="modal fade" id="rpendidikan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div id="body-form"  class="modal-body">
      <form method="POST" action="pospendidikan" class="form-horizontal" enctype="multipart/form-data">
           {{csrf_field()}}
           <input readonly hidden type="text" name="flag_edit" id="flag_edit">
          <input readonly hidden type="text" name="id" id="id">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Perguruan Tinggi</label>
            <input type="text" class="form-control" name="perguruan_tinggi" id="perguruan_tinggi">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Gelar</label>
            <input type="text" class="form-control" id="gelar" name="gelar">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Tahun Ijasah</label>
            <input type="number" class="form-control" name="tahun_ijasah" id="tahun_ijasah">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Jenjang</label>
            <input type="text" class="form-control" id="jenjang" name="jenjang">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
        </form>
    </div>
  </div>
</div>


<div class="modal fade" id="biodata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div id="body-form"  class="modal-body">
      <form method="POST" action="posbiodata" class="form-horizontal" enctype="multipart/form-data">
           {{csrf_field()}}
           <input readonly hidden type="text" name="flag_edit" id="flag_edit">
          <input readonly hidden type="text" name="id" id="id_bio">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">NIDN</label>
            <input type="text" class="form-control" name="nidn" id="nidn">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Gelar</label>
            <input type="text" class="form-control" id="gelar_bio" name="gelar">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Pendidikan</label>
            <input type="text" class="form-control" name="pendidikan" id="pendidikan">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Agama</label>
            <input type="text" class="form-control" id="agama" name="agama">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Jenis Kelamin</label>
            <input type="text" class="form-control" id="jk" name="jk">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Jabatan</label>
            <input type="text" class="form-control" id="jabatan" name="jabatan">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Tempat Lahir</label>
            <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Tanggal Lahir</label>
            <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
        </form>
    </div>
  </div>
</div>


<?php }else{ ?>
<div class="modal fade"  id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
      @foreach($d as $d)
      <form method="POST" action="posep" class="form-horizontal" id="form-tambah"  enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" class="form-control" name="id" value=" {{$d->id}} ">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">USERNAME</label>
            <input type="text" class="form-control" name="username" value=" {{Auth::user()->name}} ">
          </div>            
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">NAMA</label>
            <input type="text" class="form-control" name="name" value=" {{$d->nama}} " >
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">GELAR</label>
            <input type="text" class="form-control" name="gelar" value=" {{$d->gelar}} ">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">PENDIDIKAN</label>
            <select class="form-control" name="pendidikan">
            <option value="{{$d->pendidikan}}">{{$d->pendidikan}}</option>
            <option value="D3">D3</option>
            <option value="S1">S1</option>
            <option value="S2">S2</option>
            <option value="S3">S3</option>
            </select>
          </div>
           <div class="form-group">
            <label for="recipient-name" class="col-form-label">Agama</label>
                                <select class="form-control" name="agama" required>
                                    <option value="{{$d->agama}}">{{$d->agama}}</option>
                                    <option value="Islam">Islam</option>
                                    <option value="Khatolik">Khatolik</option>
                                    <option value="Kristen">Kristen</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Budha">Budha</option>
                                </select>
          </div>

          <div class="form-group">
            <input type="radio" name="jk" value="Laki-Laki" <?php if($d->jk == 'Laki-Laki'){echo 'checked';}?>>Laki-Laki
            <input type="radio" name="jk" value="Perempuan" <?php if($d->jk == 'Perempuan'){echo 'checked';}?>>Perempuan
          </div>

          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Tempat Lahir</label>
            <input type="text" class="form-control" name="tempat_lahir" value=" {{$d->tempat_lahir}} ">
          </div>

           <div class="form-group">
            <label for="recipient-name" class="col-form-label">Tanggal Lahir</label>
            <input type="date" class="form-control" name="tgl_lahir" value=" {{$d->tgl_lahir}} ">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
        </form>
        @endforeach
    </div>
  </div>
</div>
<?php } ?>