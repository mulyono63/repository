<!DOCTYPE HTML>
<html>
<head>
	<title>REPOSITORY</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="{{asset('cs/main.css')}}" />
	<link rel="stylesheet" href="{{asset('cs/new.css')}}" />
	<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script> 
	@include('partials.cs_fron') 
</head>
<body class="is-preload">

	<!-- Wrapper -->
	<div id="wrapper">

		<!-- Main -->
		<div id="main">
			<div class="inner">

				<!-- Header -->
				<!-- <header id="header">
					<a href="index" class="logo"><strong style="color: red">REPOSITORY</strong></a>
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-snapchat-ghost"><span class="label">Snapchat</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-medium"><span class="label">Medium</span></a></li>
					</ul>
				</header> -->

				<!-- Banner -->
				<section id="">
					<div class="content" style="margin-top: -5px;">
						<h1 class="judul">
							<span class="a">RE</span><span class="c">PO</span><span class="e">SI</span><span class="b">TO</span><span class="d">RY</span></h1>
							<!-- <img src="download.jpg" style="height: 90px;margin-bottom: -25px;"><br> -->
							<!-- <h6 style="font-size: 20px;color: blue;text-align: center;">REPOSITORY DARMAJAYA</h6> --><br>
							<form method="GET" action="search">
							{{csrf_field()}}
								<section id="search" class="alt">
									<div style="list-style-type:none !important;">
										<input type="text" class="form-control" name="keyword" id="country" placeholder="Search..." required="" autofocus="" autocomplete="off" style="list-style-type:none !important; border-radius: 90px;"/>
									</div>
								</section>
							</form>
							<h6  class="judul2">REPOSITORY JURUSAN SISTEM INFORMASI  DARMAJAYA</h6>
						</div>
					</section>

					<!-- Section -->

					<!-- Section -->

				</div>
			</div>

			<!-- Sidebar -->
			<div id="sidebar">
				<div class="inner">

					<!-- Menu -->
					@include('frontend.nav')

					<!-- Section -->
					

					<!-- Section -->
					@include('frontend.foter')
				</div>
			</div>

		</div>
        <script src="{{asset('js/browser.min.js')}}"></script>
		<script src="{{asset('js/breakpoints.min.js')}}"></script>
		<script src="{{asset('js/util.js')}}"></script>
		<script src="{{asset('js/main.js')}}"></script>
	</body>
	</html>


    