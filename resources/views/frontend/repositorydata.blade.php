<!DOCTYPE HTML>
<html>
<head>
	<title>REPOSITORY</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="{{asset('cs/main.css')}}" />
	<link rel="stylesheet" href="{{asset('cs/new.css')}}" />
	<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
	@include('partials.cs_fron')   
</head>
<body class="is-preload">

	<!-- Wrapper -->
	<div id="wrapper">

		<!-- Main -->
		<div id="main">
			<div class="inner">

				<!-- Banner -->
				<section id="">
					<div class="content" style="margin-top: -5px;">
						<h1 class="judul">
                            <h2 class="judul3"><header class="major">Title</header></h2>{{$name->judul}}</a><br>
                            <h2 class="judul3"><header class="major">Abstract</header></h2>{{$name->abstract}}</a><br>
							<?php $a=Crypt::encrypt($name->id); ?>
                            <h4 class="judul3"><a href="/login"><img  src="{{asset('img/userimg/file.png')}}">Download</h4></a><br>
                            <h4 class="judul3">Jenis Penulisan        : 
							@if(DB::select("select * from tab_penelitian where id=$name->id"))Penelitian
							@elseif(DB::select("select * from tab_skripsi where id=$name->id"))SKripsi
							@elseif(DB::select("select * from tab_jurnal where id=$name->id"))Jurnal
							@endif</h4></a>
                            <h4 class="judul3">Ditulis Oleh  : {{$name->pengarang_1}}</h4></a>
                            <h4 class="judul3">Kode Referensi 			: {{$name->id}}</h4></a>
                            <h4 class="judul3">TAG   : 
                            	@foreach($c as $c)
                            	{{$c->tag}},
                            	@endforeach

                            </h4>
                        </a>
                            <h4 class="judul3">Dosen Pembimbing 1: 
                            	@foreach($d as $dd)
                            	<a href="/dosenin/<?php echo $name->pembimbing_1 ?>">
                            	{{$dd->pem1}}
                            	</a>
                            	@endforeach

                            </h4>
                            <h4 class="judul3">Dosen Pembimbing 2: 
                            	@foreach($d as $dd)
                            	<a href="/dosenin/<?php echo $name->pembimbing_1 ?>">
                            	{{$dd->pem2}}
                            	</a>
                            	@endforeach

                            </h4>
                        </a>
                            <h4 class="judul3">Date Deposited   : {{date('d F Y', strtotime($name->created_at))}}</h4></a>
                            <h4 class="judul3">Last Modified    : {{date('d F Y', strtotime($name->updated_at))}}</h4></a>
						</div>
					</section>

					<!-- Section -->
					<!-- Section -->

				</div>
			</div>

			<!-- Sidebar -->
			<div id="sidebar">
				<div class="inner">

					<!-- Menu -->
					@include('frontend.navfront')
					
					
					<!-- Section -->
					
					
					<section>
						<header class="major">
						<h2>#STATISTIK</h2>
						</header>
						<ul class="contact">
						<li ><a href="/login"><img  src="{{asset('img/userimg/file.png')}}"></a> {{$name->judul}}</li>
                        @foreach($d as $d)
                        <li class="fa fa-eye">{{$d->view}}</li>
						<li class="fa-cloud-download">{{$d->download}}</li>
						<li class="fa-quote-left">{{$d->petik}}</li>
                        @endforeach
						</ul>
					</section>
				</div>
			</div>

		</div>
        <script src="{{asset('js/browser.min.js')}}"></script>
		<script src="{{asset('js/breakpoints.min.js')}}"></script>
		<script src="{{asset('js/util.js')}}"></script>
		<script src="{{asset('js/main.js')}}"></script>
	</body>
	</html>


    