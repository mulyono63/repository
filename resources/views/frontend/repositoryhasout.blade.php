<!DOCTYPE HTML>
<html>
<head>
	<title>REPOSITORY</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="{{asset('cs/main.css')}}" />
	<link rel="stylesheet" href="{{asset('cs/new.css')}}" />
	<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
	@include('partials.cs_fron')  
</head>
<body class="is-preload">

	<!-- Wrapper -->
	<div id="wrapper">

		<!-- Main -->
		<div id="main">
			<div class="inner">

				<!-- Banner -->
				<section id="">
					<div class="content" style="margin-top: -5px;">
						<h1 class="judul">
							<span class="a">RE</span><span class="c">PO</span><span class="e">SI</span><span class="b">TO</span><span class="d">RY</span></h1>
							<!-- <img src="download.jpg" style="height: 90px;margin-bottom: -25px;"><br> -->
							<!-- <h6 style="font-size: 20px;color: blue;text-align: center;">REPOSITORY DARMAJAYA</h6> --><br>
							<p><a href="/repositoryp">Penelitian</a>   <a href="/repositorys">Skripsi</a>   <a href="/repositoryj">Jurnal</a></p>
							@if(!empty($a == 1))
							<form method="GET" action="searchp">
							@elseif(!empty($a == 2))
							<form method="GET" action="searchs">
							@elseif(!empty($a == 3))
							<form method="GET" action="searchj">
							@else
							<form method="GET" action="search">
							@endif
							{{csrf_field()}}
								<section id="search" class="alt">
									<div style="list-style-type:none !important;">
										<input type="text" class="form-control" name="keyword" id="country" placeholder="Search..." required="" autofocus="" autocomplete="off" style="list-style-type:none !important; border-radius: 90px;"/>
									</div>
								</section>
							</form>
						</div>
					</section>

					<!-- Section -->
					@if($name)
						@foreach($name as $name)
						<?php $a=Crypt::encrypt($name->id); ?>
							<h4 class="judul3"><a href="dataout/{{$a}}">{{$name->judul}}</a></h4>
							<b><a href="/dosen/<?php echo $name->pem_1 ?>"><?php echo $name->pembimbing_1 ?></a>. ,  <a href="dosen/<?php echo $name->pem_2 ?>"><?php echo $name->pembimbing_2 ?></a>. {{date('d-m-Y', strtotime($name->created_at))}}  </b><?php echo (str_word_count($name->abstract) > 10 ? substr($name->abstract,0,800)."[...]" : $name->abstract) ?>
							</a><br>
						@endforeach
					@else
					<h4 class="judul3" >Data yang di cari tidak ditemukan...</h4>
					@endif
					<!-- Section -->

				</div>
			</div>

			<!-- Sidebar -->
			<div id="sidebar">
				<div class="inner">

					<!-- Menu -->
					@include('frontend.nav')

					<!-- Section -->
					

					<!-- Section -->
					@include('frontend.foter')
				</div>
			</div>


		</div>
        <script src="{{asset('js/browser.min.js')}}"></script>
		<script src="{{asset('js/breakpoints.min.js')}}"></script>
		<script src="{{asset('js/util.js')}}"></script>
		<script src="{{asset('js/main.js')}}"></script>
	</body>
	</html>


    