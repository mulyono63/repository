<script type="text/javascript">

	//untuk load datatable

	$(document).ready(function(){
		//untuk notifikasi berhasil save data
		var flag_save_barang = "{{ Session::get('s') }}";
		if (flag_save_barang == 1) {
			swal({
				type: 'success',
				title: 'Sukses!',
				text: 'Data barang berhasil ditambahkan',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Oke'
			});
		}
	}); //untuk penutup fungsi yang berjalan saat halaman reload

	

	//aksi klik tambah
	$('#btn-tambah').click(function(){
		$('#card-title').html("Tambah Account Dosen")
		$('#btn-tambah').fadeOut();
		$('#body-table').hide(function(){
			$('#body-form').fadeIn();
		});
	});

	//kalau batal tambah barang
	$('#btn-batal').click(function(){
		$('#card-title').html("Daftar Account Dosen");
		document.getElementById('form-tambah').reset();
		$('#btn-tambah').show();
		$('#body-form').hide(function(){
			$('#body-table').fadeIn();
		});
	});

	//aksi ketika klik button edit
	function editpendidikan(id,id_dosen,perguruan_tinggi,gelar,tahun_ijasah,jenjang){
		//flag edit ini dipakai biar satu form bisa untuk edit sama tambah
		$('#flag_edit').val(1);

		//isi formm dari data yang di kurung fungsi
		// document.getElementById('tag').readOnly = true;
		$('#id').val(id);
		$('#id_dosen').val(id_dosen);
		$('#perguruan_tinggi').val(perguruan_tinggi);
		$('#gelar').val(gelar);
		$('#tahun_ijasah').val(tahun_ijasah);
		$('#jenjang').val(jenjang);


		//habis form diisi, formnya tampil
		$('#body-table').hide(function(){
			$('#body-form').fadeIn();
		});
	}

	function editbiodata(id,nip,nama,gelar,pendidikan,agama,jk,nidn,jabatan,tempat_lahir,tgl_lahir){
		//flag edit ini dipakai biar satu form bisa untuk edit sama tambah
		$('#flag_edit').val(1);

		//isi formm dari data yang di kurung fungsi
		// document.getElementById('tag').readOnly = true;
		$('#id_bio').val(id);
		$('#nama').val(nama);
		$('#gelar_bio').val(gelar);
		$('#pendidikan').val(pendidikan);
		$('#agama').val(agama);
		$('#jk').val(jk);
		$('#nidn').val(nidn);
		$('#jabatan').val(jabatan);
		$('#tempat_lahir').val(tempat_lahir);
		$('#tgl_lahir').val(tgl_lahir);


		//habis form diisi, formnya tampil
		$('#body-table').hide(function(){
			$('#body-form').fadeIn();
		});
	}
</script>