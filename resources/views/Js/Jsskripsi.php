<script type="text/javascript">

	//untuk load datatable

	$(document).ready(function(){
		//untuk notifikasi berhasil save data
		var flag_save_barang = "{{ Session::get('s') }}";
		if (flag_save_barang == 1) {
			swal({
				type: 'success',
				title: 'Sukses!',
				text: 'Data barang berhasil ditambahkan',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Oke'
			});
		}
	}); //untuk penutup fungsi yang berjalan saat halaman reload

	

	//aksi klik tambah barang
	$('#btn-tambah').click(function(){
		$('#card-title').html("Tambah Data Skripsi")
		$('#btn-tambah').fadeOut();
		$('#body-table').hide(function(){
			$('#body-form').fadeIn();
		});
	});

	//kalau batal tambah barang
	$('#btn-batal').click(function(){
		$('#card-title').html("Data Skripsi");
		document.getElementById('form-tambah').reset();
		$('#btn-tambah').show();
		$('#body-form').hide(function(){
			$('#body-table').fadeIn();
		});
	});

	//aksi ketika klik button edit
	function editskripsi(id,judul,abstract,pengarang_1,id_tag,halaman,pembimbing_1,pembimbing_2,file){
		//flag edit ini dipakai biar satu form bisa untuk edit sama tambah
		$('#flag_edit').val(1);

		//isi formm dari data yang di kurung fungsi
		// document.getElementById('tag').readOnly = true;
		$('#id').val(id);
		$('#id_taga').hide();
		document.getElementById('id_tag').disabled = true;
		$('#judul').val(judul);
		$('#abstract').val(abstract);
		$('#pengarang_1').val(pengarang_1);
		$('#id_tag').val(id_tag);
		$('#halaman').val(halaman);
		$('#pembimbing_1').val(pembimbing_1);
		$('#pembimbing_2').val(pembimbing_2);
		$('#file').val(file);
		document.getElementById('petik_skripsi').disabled = true;
		$('#petik_skripsia').hide();

		//habis form diisi, formnya tampil
		$('#card-title').html("Edit Data Skripsi")
		$('#btn-tambah').fadeOut();
		$('#body-table').hide(function(){
			$('#body-form').fadeIn();
		});
	}

	//aksi ketika klik button Validasi
	function proses(id,judul,abstract,pengarang_1,id_tag,halaman,pembimbing_1,pembimbing_2,file){
		//flag edit ini dipakai biar satu form bisa untuk edit sama tambah
		$('#flag_edit').val(2);
		$('#validasi').val(1);

		//isi formm dari data yang di kurung fungsi
		document.getElementById('id_tag').disabled = true;
		document.getElementById('petik_skripsi').disabled = true;
		document.getElementById('pembimbing_1').disabled = true;
		document.getElementById('pembimbing_2').disabled = true;
		document.getElementById('judul').disabled = true;
		document.getElementById('abstract').disabled = true;
		document.getElementById('pengarang_1').disabled = true;
		document.getElementById('halaman').disabled = true;
		$('#id').val(id);
		$('#id_taga').hide();
		$('#pembimbing_1').val(pembimbing_1);
		$('#pembimbing_2').val(pembimbing_2);
		$('#judul').val(judul);
		$('#abstract').val(abstract);
		$('#pengarang_1').val(pengarang_1);
		$('#halaman').val(halaman);
		$('#file').val(file);

		//habis form diisi, formnya tampil
		$('#card-title').html("Validasi Data Skripsi")
		$('#btn-tambah').fadeOut();
		$('#body-table').hide(function(){
			$('#body-form').fadeIn();
			$('#validasi').fadeIn();
			$('#btn-save').hide();
			$('#fil').hide();
			$('#kut').hide();
			$('#btn-batal').fadeIn();
		});
	}
</script>