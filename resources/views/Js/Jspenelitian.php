<script type="text/javascript">

	//untuk load datatable

	$(document).ready(function(){
		//untuk notifikasi berhasil save data
		var flag_save_barang = "{{ Session::get('s') }}";
		if (flag_save_barang == 1) {
			swal({
				type: 'success',
				title: 'Sukses!',
				text: 'Data barang berhasil ditambahkan',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Oke'
			});
		}
	}); //untuk penutup fungsi yang berjalan saat halaman reload

	

	//aksi klik tambah barang
	$('#btn-tambah').click(function(){
		$('#card-title').html("Tambah Data Penelitian")
		$('#btn-tambah').fadeOut();
		$('#body-table').hide(function(){
			$('#body-form').fadeIn();
		});
	});

	//kalau batal tambah barang
	$('#btn-batal').click(function(){
		$('#card-title').html("Daftar Data Penelitian");
		document.getElementById('form-tambah').reset();
		$('#btn-tambah').show();
		$('#body-form').hide(function(){
			$('#body-table').fadeIn();
		});
	});


	//aksi ketika klik button edit
	function editpenelitian(id,judul,abstract,pengarang_1,pengarang_2,pengarang_3,halaman,pembimbing_1,pembimbing_2,file){
		//flag edit ini dipakai biar satu form bisa untuk edit sama tambah
		$('#flag_edit').val(1);

		//isi formm dari data yang di kurung fungsi
		// document.getElementById('tag').readOnly = true;
		$('#id').val(id);
		$('#pembimbing_1').val(pembimbing_1);
		$('#pembimbing_2').val(pembimbing_2);
		$('#judul').val(judul);
		$('#abstract').val(abstract);
		$('#pengarang_1').val(pengarang_1);
		$('#pengarang_2').val(pengarang_2);
		$('#pengarang_3').val(pengarang_3);
		$('#halaman').val(halaman);
		$('#file').val(file);
		document.getElementById('petik_penelitian').disabled = true;
		$('#petik_penelitiana').hide();


		//habis form diisi, formnya tampil
		$('#card-title').html("Edit Data Penelitian")
		$('#btn-tambah').fadeOut();
		$('#body-table').hide(function(){
			$('#body-form').fadeIn();
		});
	}

	//aksi ketika klik button Validasi
	function proses(id,judul,abstract,pengarang_1,pengarang_2,pengarang_3,halaman,pembimbing_1,pembimbing_2,file){
		//flag edit ini dipakai biar satu form bisa untuk edit sama tambah
		$('#flag_edit').val(2);
		$('#validasi').val(1);

		//isi formm dari data yang di kurung fungsi
		document.getElementById('pembimbing_1').disabled = true;
		document.getElementById('pembimbing_2').disabled = true;
		document.getElementById('judul').disabled = true;
		document.getElementById('abstract').disabled = true;
		document.getElementById('pengarang_1').disabled = true;
		document.getElementById('pengarang_2').disabled = true;
		document.getElementById('pengarang_3').disabled = true;
		document.getElementById('halaman').disabled = true;
		document.getElementById('petik_penelitian').disabled = true;
		$('#petik_penelitiana').hide();
		$('#id').val(id);
		$('#pembimbing_1').val(pembimbing_1);
		$('#pembimbing_2').val(pembimbing_2);
		$('#judul').val(judul);
		$('#abstract').val(abstract);
		$('#pengarang_1').val(pengarang_1);
		$('#pengarang_2').val(pengarang_2);
		$('#pengarang_3').val(pengarang_3);
		$('#halaman').val(halaman);
		$('#file').val(file);

		//habis form diisi, formnya tampil
		$('#card-title').html("Validasi Data Penelitian")
		$('#btn-tambah').fadeOut();
		$('#body-table').hide(function(){
			$('#body-form').fadeIn();
			$('#validasi').fadeIn();
			$('#btn-save').hide();
			$('#fil').hide();
			$('#kut').hide();
			$('#btn-batal').fadeIn();
		});
	}
</script>