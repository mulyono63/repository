<script type="text/javascript">

	//untuk load datatable

	$(document).ready(function(){
		//untuk notifikasi berhasil save data
		var flag_save_barang = "{{ Session::get('s') }}";
		if (flag_save_barang == 1) {
			swal({
				type: 'success',
				title: 'Sukses!',
				text: 'Data barang berhasil ditambahkan',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Oke'
			});
		}
	}); //untuk penutup fungsi yang berjalan saat halaman reload

	

	//aksi klik tambah barang
	$('#btn-tambah').click(function(){
		$('#card-title').html("Tambah Barang")
		$('#btn-tambah').fadeOut();
		$('#body-table').hide(function(){
			$('#body-form').fadeIn();
		});
	});

	//kalau batal tambah barang
	$('#btn-batal').click(function(){
		$('#card-title').html("Daftar Barang");
		document.getElementById('form-tambah').reset();
		document.getElementById('kode_barang').readOnly = false;
		$('#btn-tambah').show();
		$('#body-form').hide(function(){
			$('#body-table').fadeIn();
		});
	});
</script>