<script type="text/javascript">

	//untuk load datatable

	$(document).ready(function(){
		//untuk notifikasi berhasil save data
		var flag_save_barang = "{{ Session::get('s') }}";
		if (flag_save_barang == 1) {
			swal({
				type: 'success',
				title: 'Sukses!',
				text: 'Data barang berhasil ditambahkan',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Oke'
			});
		}
	}); //untuk penutup fungsi yang berjalan saat halaman reload

	

	//aksi klik tambah osen
	$('#btn-tambah').click(function(){
		$('#card-title').html("Tambah Account Dosen")
		$('#btn-tambah').fadeOut();
		$('#body-table').hide(function(){
			$('#body-form').fadeIn();
		});
	});

	//kalau batal tambah dosen
	$('#btn-batal').click(function(){
		$('#card-title').html("Daftar Account Dosen");
		document.getElementById('form-tambah').reset();
		$('#btn-tambah').show();
		$('#body-form').hide(function(){
			$('#body-table').fadeIn();
		});
	});

		//aksi ketika klik button edit dosen
	function editdosen(id,tag){
		//flag edit ini dipakai biar satu form bisa untuk edit sama tambah
		$('#flag_edit').val(1);

		//isi formm dari data yang di kurung fungsi
		// document.getElementById('tag').readOnly = true;
		$('#tag').val(tag);
		$('#id').val(id);

		//habis form diisi, formnya tampil
		$('#card-title').html("Edit Data TAG")
		$('#btn-tambah').fadeOut();
		$('#body-table').hide(function(){
			$('#body-form').fadeIn();
		});
	}

	//aksi ketika klik button hapus barang
	function hapusdosen(id){
		
				window.location.replace("/deletetag/"+id);
		
	}
</script>