<!doctype html>
 <!DOCTYPE html>
<html lang="en">

<head>
    <title>Login</title>
    <!-- Meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Allied Login Form Responsive Widget, Audio and Video players, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"
    />
    <script>
        addEventListener("load", function () { setTimeout(hideURLbar, 0); }, false); function hideURLbar() { window.scrollTo(0, 1); }
    </script>
    <!-- Meta tags -->
    <!-- font-awesome icons -->
    <link href="{{asset('cs/font-awesome.min.css')}}" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!--stylesheets-->
    <link href="{{asset('cs/style.css')}}" rel='stylesheet' type='text/css' media="all">
    <!--//style sheet end here-->
    <link href="{{asset('cs/font1.css')}}" rel="stylesheet">
    <link href="{{asset('cs/font2.css')}}" rel="stylesheet">
    @include('partials.cs')
</head>

<body>
    <h1 class="error"></h1>
    <div class="w3layouts-two-grids">
        <div class="mid-class">
            <div class="txt-left-side">
                <h2 style="color: white;"> Login Here </h2>
                <br>
                @if ($message = Session::get('b'))
                    <div style="color:red">
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                <form action="postlogin" method="POST">
                {{csrf_field()}}
                    <div class="form-left-to-w3l">
                        <span class="fa fa-envelope-o" aria-hidden="true"></span>
                        <input type="email" name="email" placeholder="Email Address" required="">

                        <div class="clear"></div>
                    </div>
                    <div class="form-left-to-w3l ">

                        <span class="fa fa-lock" aria-hidden="true"></span>
                        <input type="password" name="password" placeholder="Password" required="">
                        <div class="clear"></div>
                        <div style="color:red">
                        <strong>{{ session::get('a') }}</strong>
                        </div>
                    </div>
                    
                    <div class="main-two-w3ls">
                        <div class="left-side-forget">
                            <a href="/lupa"><span style="color:white">Lupa Password </span></a>
                        </div>
                       <!--  <div class="right-side-forget">
                            <a href="#" class="for">Forgot password...?</a>
                        </div> -->
                    </div>
                    <div class="btnn">
                        <button type="submit" name="kirim">Login </button>
                    </div>
                </form>
                <div class="w3layouts_more-buttn">
                <h6 style="color:white">Create an account?</h6><a href='daftar'> Sign up</a>
                    <!-- <h3>REPOSITORY SISTEM INFORMASI
                        <a href="#">DARMAJAYA<br>2019
                        </a>
                    </h3> -->
                </div>

            </div>
            <div class="img-right-side">
                <h3>Welcome To Repository Login Form</h3>
                <p>Repository Jurusan Sistem Informasi ibb darmajaya</p>
                <img src="css/images/b11.png" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</body>

</html>