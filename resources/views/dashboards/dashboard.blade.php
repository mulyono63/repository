<div class="right_col" role="main">

			
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="view-header">
                            <div class="pull-right text-right" style="line-height: 14px">
                                <small>REPOSITORY<br>Dashboard<br> <span class="c-white">v.1.7</span></small>
                            </div>
                            <div class="header-icon">
                                <i class="pe page-header-icon pe-7s-study"></i>
                            </div>
                            <div class="header-title">
                                <h3 class="m-b-xs">REPOSITORY</h3>
                                <small>
                                    Layanan sistem informasi jejaring akademik mahasiswa dan dosen.
                                </small>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

	

           <div class="row">
                <div class="col-md-12 animated fadeInUp">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <div class="row">	
								<div class="col-md-4">

                                <h1 class="m-t-sm m-b-sm">
                                    <i class="pe pe-7s-global text-warning"> </i>
                                    <a href="index.php?cm=akademik&amp;items=1&amp;a=reset" target="_top">2018/2019</a>  <span class="smedium"><i class="fa fa-play fa-rotate-270 text-success"> </i> Genap</span>
                                </h1>
                                <div class="small">
                                    <span class="c-accent">Tahun Akademik</span> yang saat ini sedang aktif pada sistem informasi akademik. Tahun pelaporan : <span class="text-primary font-bold">2018</span>.
                                </div>
								</div>							
                                <div class="col-md-8">
									<div class="media">
											<div class="slight m-t-sm"><div class="font-bold m-t-xs">....</div>DIREKTUR</div>
											<div class="slight m-t-xs"><div class="font-bold m-t-xs">...</div>PEMBANTU DIREKTUR I </div>													                            
									</div>								
                                    
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>							
	

	               

                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12 animated fadeInUp">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Info/Pengumuman <small>Akademik</small></h2>
		                                <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">										
										<div class="dashboard-widget-content">
										<div id="load_content1">


                                            <div id="result"></div></div>
										</div>											
                                    </div>
                                </div>
                            </div>
                        </div>
						
                        <div class="row hidden">
                            <!-- Start to do list -->
                            <div class="col-md-6 col-sm-6 col-xs-12 animated fadeInUp">
                                							<div class="x_panel">
								<div class="x_title">
									<h2>Recent Activities <small>Sessions</small></h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
							   
								</div>
							</div>
                            </div>
                            <!-- End to do list -->


                            <!-- start of weather widget -->
                            <div class="col-md-6 col-sm-6 col-xs-12 animated fadeInUp">
                                <div class="x_panel">
								<div class="x_title">
									<h2>Recent Activities <small>Sessions</small></h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
							   
								</div>
							</div>

                            </div>
                            <!-- end of weather widget -->
                        </div>

                    </div>
					
					
                    <div class="col-md-4 col-sm-4 col-xs-12">
					<div class="row">
					
					
					
					    <div class="col-md-12 col-sm-12 col-xs-12 animated fadeInUp">
							<div class="x_panel">
								<div class="x_title">
									<h2>Dokumen <small>Akademik</small></h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
										<div class="">
                                            <ul class="to_do">
                                                <!-- <li><p><i class="fa fa-file-pdf-o"></i> <a href="index.php?cm=dok&amp;items=1&amp;a=reset" target="_top">Peraturan Akademik</a></p></li>
												<li><p><i class="fa fa-file-pdf-o"></i> <a href="index.php?cm=dok&amp;items=2&amp;a=reset" target="_top">Kode Etik</a></p></li>
												<li><p><i class="fa fa-file-pdf-o"></i> <a href="index.php?cm=dok&amp;items=3&amp;a=reset" target="_top">Kalender Akademik 2018/2019</a></p></li>
                                                <li><p><i class="fa fa-file-pdf-o"></i> Buku Panduan Proyek Mandiri</p></li>
                                                <li><p><i class="fa fa-file-pdf-o"></i> Buku Panduan PKL</p></li>
                                                <li><p><i class="fa fa-file-pdf-o"></i> Buku Panduan Tugas Akhir / Skripsi</p></li> -->
                                            </ul>
                                        </div>							   
								</div>
							</div>
						</div>

						
					</div>	
                    </div>					
					
					

                </div>




									
            </div>