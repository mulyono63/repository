<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="/search_in" class="site_title"><i class="fa fa-book"></i> <span>REPOSITORY</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="{{asset('img/userimg/'.Auth::user()->foto)}}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{Auth::user()->name}}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                <li><a href="search_in" ><i class="fa fa-home"></i> Beranda</a></li>
                <li><a href="/statistik"><i class="fa fa-bar-chart"></i> Statistik </a></li>
                  <li><a ><i class="fa fa-list"></i> Data Repository<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <?php $id_user= Auth::user()->id; ?>
                    @if(Auth::user()->role == 'admin')
                      <?php $co_p= DB::select("select Count(id) as a from tab_penelitian where active_p='0'  "); ?>
                      <?php $co_s= DB::select("select Count(id) as a from tab_skripsi where active_s='0'"); ?>
                      <?php $co_j= DB::select("select Count(id) as a from tab_jurnal where active_j='0'"); ?>
                        <li><a href="penelitian">PENELITIAN
                        @foreach($co_p as $co_p)
                      @if($co_p->a=='0')<span class="badge bg-red">@else <span class="badge bg-green"> @endif{!!$co_p->a!!}</span>
                        @endforeach
                        </a></li>
                        <li><a href="skripsi">SKRIPSI
                        @foreach($co_s as $co_s)
                        @if($co_s->a=='0')<span class="badge bg-red">@else <span class="badge bg-green"> @endif{!!$co_s->a!!}</span>
                        @endforeach
                        </a></li>
                        <li><a href="jurnal">JURNAL
                        @foreach($co_j as $co_j)
                        @if($co_j->a=='0')<span class="badge bg-red">@else <span class="badge bg-green"> @endif{!!$co_j->a!!}</span>
                        @endforeach
                        </a></li>
                    @elseif(Auth::user()->role == 'dosen')
                    <?php $npm = Auth::user()->npm; ?>
                        <?php $cou_p= DB::select("select Count(id) as a from tab_penelitian where active_p=2 and id_user=$id_user  "); ?>
                        <?php $co_s= DB::select("select Count(id) as a from tab_skripsi where active_s='0' and (pembimbing_1 = $npm or pembimbing_2 = $npm)"); ?>
                        <?php $co_j= DB::select("select Count(id) as a from tab_jurnal where active_j='0' and (pembimbing_1 = $npm or pembimbing_2 = $npm)"); ?>
                         <li><a href="penelitian">PENELITIAN
                        @foreach($cou_p as $co_p)
                        @if($co_p->a=='0')<span class="badge bg-red">@else <span class="badge bg-green"> @endif{!!$co_p->a!!}</span>
                        @endforeach
                        </a></li>
                        <li><a href="skripsi">SKRIPSI
                        @foreach($co_s as $co_s)
                        @if($co_s->a=='0')<span class="badge bg-red">@else <span class="badge bg-green"> @endif{!!$co_s->a!!}</span>
                        @endforeach
                        </a></li>
                        <li><a href="jurnal">JURNAL
                        @foreach($co_j as $co_j)
                        @if($co_j->a=='0')<span class="badge bg-red">@else <span class="badge bg-green"> @endif{!!$co_j->a!!}</span>
                        @endforeach
                        </a>
                      </li>
                    @else
                        <?php $cou_s= DB::select("select Count(id) as a from tab_skripsi where active_s=2 and id_user=$id_user "); ?>
                          <li><a href="skripsi">SKRIPSI
                          @foreach($cou_s as $cou_s)
                          @if($cou_s->a=='0')<span class="badge bg-red">@else <span class="badge bg-green"> @endif{!!$cou_s->a!!}</span>
                          @endforeach
                          </a></li>
                          <?php $cou_j= DB::select("select Count(id) as a from tab_jurnal where active_j=2 and id_user=$id_user  "); ?>

                          <li><a href="jurnal">JURNAL
                          @foreach($cou_j as $cou_j)
                          @if($cou_j->a=='0')<span class="badge bg-red">@else <span class="badge bg-green"> @endif{!!$cou_j->a!!}</span>
                          @endforeach
                          </a></li>
                    @endif
                    </ul>
                  </li>
                  @if(Auth::user()->role == 'admin')
                  <li><a ><i class="fa fa-user"></i> Data User<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="dosen">Dosen</a></li>
                      <li><a href="mahasiswa">Mahasiswa</a></li>
                    </ul>
                  </li>
                  
                  <li><a ><i class="fa fa-edit"></i>Data TAG<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="tag">TAG</a></li>
                    </ul>
                  </li>
                  @elseif(Auth::user()->role == 'dosen')
                  <li>
                    <a href="datadocumen" ><i class="fa fa-folder"></i>Document</a>
                  </li>
                  @endif
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <!-- /menu footer buttons -->
          </div>
        </div>