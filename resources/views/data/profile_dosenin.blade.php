<!DOCTYPE HTML>
<html>

<head>
  <title>REPOSITORY</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="{{asset('cs/main.css')}}" />
  <link rel="stylesheet" href="{{asset('cs/new.css')}}" />
  <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script> 
  @include('partials.cs_fron') 
<style type="text/css">
  .head{
  color:#1a2028;
  text-align: center;
}
.Tabs{
  padding-top: 50px;
  width:100%;
  height:300px;
}
.Tabs .TabMenu{
  height: 15%;
}
.Tabs .TabMenu button{
  width: 25%;
  height: 100%;
  float: left;
  border:none;
  outline: none;
  cursor: pointer;
  padding: 10px;
  font-size: 16px;
  font-weight: bold;
  background-color: #96ea2e;
}
.Tabs .TabMenu button:hover{
  background-color:#57db7a ;
}
.Tabs .tabPanel{
  height : 200%;
  width: 100%;
  background: #cccfff;
  text-align: center;
  padding-top: 50px;
  box-sizing: border-box;
  font-size: 18px;
  display: none;
}
</style>
</head>
<body class="is-preload">

  <!-- Wrapper -->
  <div id="wrapper">

    <!-- Main -->
    <div id="main">
      <div class="inner">


        <!-- Banner -->
      <div class="Tabs">
    <div class="TabMenu">
      <button onclick="showPanel(0)">BiodData Dosen</button>
      <button onclick="showPanel(1)">Pendidikan</button>
      <button onclick="showPanel(2)">Penelitian</button>
      <button onclick="showPanel(3)">Telah Membimbing</button>
    </div>
  <div class="tabPanel">
          <?php 
           foreach($f as $a){
            ?>
        <img style="width: 250px; height: 250px;padding-bottom: 10px; border-radius:50%;" src="{{asset('img/userimg/'.$a->foto)}}"  alt=""  > 
          <?php } ?>
    <ul>
    <li>
      <table id="datatable" class="table table-striped table-bordered">
        <thead>
         <tr>
            <th>No</th>
            <th>NIDN</th>
            <th>Dosen</th>
            <th>Jabatan Fungsional</th>
            <th>Tempat/Tgl Lahir</th>
            <th>Jenis Kelamin</th>
            <th>Pendidikan Tinggi</th>
        </tr>
        </thead>
        <tbody>
           <?php $no=0;
           foreach($d as $a){
           $no++; ?>
           <tr>
             <th scope="row">{{$no}}</th>
             <td>{{$a->nidn}}</td>
             <td>{{$a->nama}}, {{$a->gelar}}</td>
             <td>{{$a->jabatan}}</td>
             <td>{{$a->tempat_lahir}}, {{$a->tgl_lahir}}</td>
             <td>{{$a->jk}}</td>
             <td>{{$a->pendidikan}}</td>
           </tr>
           <?php } ?>
         </tbody>
      </table>
    </li>
  </ul>
</nav>
</div>


<div class="tabPanel">
<table id="datatable" class="table table-striped table-bordered">
         <thead>
           <tr>
             <th>No</th>
             <th>Perguruan Tinggi</th>
             <th>Gelar Akademik</th>
             <th>Tahun Ijazah</th>
             <th>Jenjang</th>
           </tr>
         </thead>
         <tbody>
          <?php if(!empty($b)){ ?>
           <?php $no=0;
            foreach($b as $a){
            $no++; ?>
           <tr>
             <th scope="row">{{$no}}</th>
             <td>{{$a->perguruan_tinggi}}</td>
             <td>{{$a->gelar}}</td>
             <td>{{$a->tahun_ijasah}}</td>
             <td>{{$a->jenjang}}</td>
           </tr>
           <?php } ?>
           <?php }else{ ?>
            <tr>
              <td colspan="6">Tidak Ada Data Yang Di Tampilkan</td>
            </tr>
           <?php } ?>
         </tbody>
       </table>
</div>

<div class="tabPanel">
<table id="datatable" class="table table-striped table-bordered">
      <thead>
        <tr >
            <th>No</th>
            <th>Judul</th>
            <th>Halaman</th>
            <th>Pengarang</th>
            <th> <h4><i class="fa fa-eye"></i></h4> </th>
            <th><h4><i class="fa fa-cloud-download"></i></h4></th>
            <th>Kutipan</th>
        </tr>
    </thead>
    <tbody>
    <?php $a=0; ?>
      @foreach($e as $e)
    <?php $a++; ?>
    <tr>
    <td nowrap>{{$a}}</td>
        <td>{{$e->judul}}</td>
            <td>{{$e->halaman}}</td>
            <td>{{$e->pengarang_1}} / </br> {{$e->pengarang_2}} </br>/ {{$e->pengarang_3}}</td>
            <td>{{$e->view_penelitian}}</td>
            <td>{{$e->download_penelitian}}</td>
            <td>{{$e->petik_penelitian}}</td>
    </tr>
    @endforeach
    </tbody>
      </table>
</div>

<div class="tabPanel">
<table id="datatable" class="table table-striped table-bordered">
    <thead>
     <tr>
       <th>No</th>
       <th>Mahasiswa</th>
       <th>Total Skripsi</th>
       <th>Total Jurnal</th>
       <th>Tottal Penelitian</th>
     </tr>
   </thead>
   <tbody>
    <?php if(!empty($g)){ ?>
     <?php $no=0;
      foreach($g as $a){
      $no++; ?>
     <tr>
       <th scope="row">{{$no}}</th>
       <td>Mahasiswa</td>
       <td>{{$a->total_skripsi}}</td>
       <td>{{$a->total_jurnal}}</td>
       <td>{{$a->total_penelitian}}</td>
     </tr>
     <?php } ?>
     <?php }else{ ?>
      <tr>
        <td colspan="5">Tidak Ada Data Yang Di Tampilkan</td>
      </tr>
      <?php } ?>
   </tbody>
  </table>
</div>
</div>
          <!-- Section -->

          <!-- Section -->

        </div>
      </div>

      <!-- Sidebar -->
      <div id="sidebar">
        <div class="inner">

          <!-- Menu -->
          @include('frontend.navfront')

          <!-- Section -->
          

          <!-- Section -->
          @include('frontend.foter')
        </div>
      </div>

    </div>
        <script src="{{asset('js/browser.min.js')}}"></script>
    <script src="{{asset('js/breakpoints.min.js')}}"></script>
    <script src="{{asset('js/util.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script type="text/javascript">
          var tabButtons=document.querySelectorAll(".Tabs .TabMenu button");
    var tabPanels=document.querySelectorAll(".Tabs .tabPanel");

    function showPanel(panelIndex,colorCode){
      tabButtons.forEach(function(node){
        node.style.backgroundColor="";
        node.style.color="";
      });
      tabButtons[panelIndex].style.backgroundColor=colorCode;
      tabButtons[panelIndex].style.color="#662f00";
      tabPanels.forEach(function(node){
        node.style.display="none";
      });
      tabPanels[panelIndex].style.display="block";
      tabPanels[panelIndex].style.backgroundColor=colorCode;
    }
    </script>
  </body>
  </html>


    