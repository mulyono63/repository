<div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 id="card-title">Daftar Data Penelitian</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="float-right">
                  @foreach ($errors->all() as $error)
                 <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
                @if(Auth::user()->role=='admin')
                @else
                        <button id="btn-tambah" class="btn btn-primary btn-sm"><strong><i class="icon dripicons-plus" style="color: white;"></i>Tambah Data Penelitian</strong></button>
                @endif
                  </div>
                    <div id="body-table">
                    <table id="datatable" class="table table-striped table-bordered">
                    <thead bgcolor="black">
                                <tr style="color:#FFFFFF">
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>Halaman</th>
                                    <th>Pengarang</th>
                                    <th> <h4><i class="fa fa-eye"></i></h4> </th>
                                    <th><h4><i class="fa fa-cloud-download"></i></h4></th>
                                    <th>Kutipan</th>
                                    <th>Status</th>
                                    <th>File</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $a=0; ?>
                            @foreach($d as $d)
                            <?php $a++; ?>
                            <tr>
                            <td nowrap>{{$a}}</td>
									<td>{{$d->judul}}</td>
									<td>{{$d->halaman}}</td>
									<td>{{$d->pengarang_1}} / </br> {{$d->pengarang_2}} </br>/ {{$d->pengarang_3}}</td>
                                    <td>{{$d->view_penelitian}}</td>
                                    <td>{{$d->download_penelitian}}</td>
                                    <td>{{$d->petik_penelitian}}</td>
									<td>
                                    @if($d->active_p==1)
                                    <h2><span class="label label-success">Berhasil</span></h2>
                                    @elseif($d->active_p==0)
                                    <h2><span class="label label-warning">Menungu...</span></h2>
                                    @else
                                    <h2><span class="label label-danger">Ditolak...</span></h2>
                                    @endif
                                    </td>
                                    @if($d->active_p==1)
									<td><a href="{{asset('data_file/'.$d->file)}}"><img  src="{{asset('img/userimg/file.png')}}"></a></td>
                                    @elseif($d->active_p==0)
									<td><a href="{{asset('data_file/'.$d->file)}}"><img  src="{{asset('img/userimg/file.png')}}"></a></td>
                                    @else
                                    <td></td>
                                    @endif
                                    <td>
                                    @if(Auth::user()->role=='admin')
                                        @if($d->active_p==1)
                                        <a href="deletepenelitian/{{$d->id}}/{{$d->file}}" class="btn btn-danger btn-sm">Hapus</a>
                                        @elseif($d->active_p==2)
                                        <a href="deletepenelitian/{{$d->id}}/{{$d->file}}" class="btn btn-danger btn-sm">Hapus</a>
                                        @else
                                       <button class="btn btn-success btn-sm" onclick="proses(`{{$d->id}}`,`{{$d->judul}}`,`{{$d->abstract}}`,`{{$d->pengarang_1}}`,`{{$d->pengarang_2}}`,`{{$d->pengarang_3}}`,`{{$d->halaman}}`,`{{$d->pembimbing_1}}`,`{{$d->pembimbing_2}}`,`{{$d->file}}`)"><strong>validasi</strong></button>
                                        @endif
                                    @elseif(Auth::user()->role=='dosen')
                                       <button class="btn btn-success btn-sm" onclick="editpenelitian(`{{$d->id}}`,`{{$d->judul}}`,`{{$d->abstract}}`,`{{$d->pengarang_1}}`,`{{$d->pengarang_2}}`,`{{$d->pengarang_3}}`,`{{$d->halaman}}`,`{{$d->pembimbing_1}}`,`{{$d->pembimbing_2}}`,`{{$d->file}}`)"><strong>Edit</strong></button>
                                        @if($d->active_p==1)
                                        <a href="deletepenelitian/{{$d->id}}/{{$d->file}}" class="btn btn-danger btn-sm">Hapus</a>
                                        @elseif($d->active_p==0)
                                        <a href="deletepenelitian/{{$d->id}}/{{$d->file}}" class="btn btn-danger btn-sm">Hapus</a>
                                        @else
                                        <a href="deletepenelitian/{{$d->id}}/{{$d->file}}" class="btn btn-danger btn-sm">Hapus</a>
                                        @endif
                                    @endif
                                    </td>
									
								</tr>
                            @endforeach
                            
                              
                            </tbody>
                    </table>
                  </div>
                  <div id="body-form" style="display: none">
                        <form method="POST" action="postpenelitian" class="form-horizontal" id="form-tambah"  enctype="multipart/form-data">
                        {{csrf_field()}}
                            <input readonly hidden type="text" name="flag_edit" id="flag_edit">
                            <input readonly hidden type="text" name="file_del" id="file">
                            <input readonly hidden type="text" name="id" id="id">
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Judul Penelitian</label>
                                    <div class="col-md-5">
                                        <input id="judul" type="text" name="judul" placeholder="Judul Penelitian"class="form-control" value="{{ old('judul') }}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Abstract</label>
                                    <div class="col-md-5">
                                        <textarea class="form-control" rows="3" name="abstract" id="abstract" value="{{old('abstract')}}" placeholder="Abstract Penelitian" required></textarea>                                    
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Pengarang 1</label>
                                    <div class="col-md-5">
                                        <input id="pengarang_1" type="text" name="pengarang_1" placeholder="Pengarang"class="form-control" value="{{ old('pengarang_1') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body" id="peng_2">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Pengarang 2</label>
                                    <div class="col-md-5">
                                        <input id="pengarang_2" type="text" name="pengarang_2" placeholder="Pengarang"class="form-control" value="{{ old('pengarang_2') }}" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-body" id="peng_3">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Pengarang 3</label>
                                    <div class="col-md-5">
                                        <input id="pengarang_3" type="text" name="pengarang_3" placeholder="Pengarang"class="form-control" value="{{ old('pengarang_3') }}" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Halaman</label>
                                    <div class="col-md-5">
                                        <input id="halaman" type="text" name="halaman" placeholder="Halaman"class="form-control" value="{{ old('halaman') }}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Dosen Pembimbing 1</label>
                                    <div class="col-md-5">
                                       <select name="pembimbing_1" id="pembimbing_1" class="form-control" required>
                                           <option value="">-PILIH-</option>
                                           @foreach($b as $a)
                                            <option value="{{$a->nip}}">{{$a->nama}}, {{$a->gelar}}</option>
                                           @endforeach
                                       </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Dosen Pembimbing 2</label>
                                    <div class="col-md-5">
                                       <select name="pembimbing_2" id="pembimbing_2" class="form-control">
                                           <option value="">-PILIH-</option>
                                           @foreach($b as $a)
                                            <option value="{{$a->nip}}">{{$a->nama}}, {{$a->gelar}}</option>
                                           @endforeach
                                       </select>
                                    </div>
                                </div>
                            </div>

                            <div  class="form-body" >
                                <div id="petik_penelitiana" class="form-group row">
                                    <label class="control-label text-right col-md-3">Kutipan</label>
                                    <div class="col-md-5">
                                    <div class="input-group control-group after-add-more">
                                      <input type="number" name="petik_penelitian[]"  id="petik_penelitian" class="form-control" placeholder="Kutipan" required>
                                      <div class="input-group-btn"> 
                                        <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Tmabah</button>
                                      </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body" id="fil">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">File Penelitian</label>
                                    <div class="col-md-5">
                                         <input id="file" type="file" value="" name="file" class="form-control" >
                                        <span>Frotmat file pdf size 5 MB</span>
                                    </div>
                                </div>
                            </div>
                            @if(Auth::user()->role=='admin')
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Status</label>
                                    <div class="col-md-5">
                                       <select name="status_p"  class="form-control" required >
                                           <option value="">-PILIH-</option>
                                            <option value="1">Lolos</option>
                                            <option value="2">Tidak Lolos</option>
                                       </select>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3"></label>
                                    <div class="col-md-5">
                                        <button type="submit" class="btn btn-info" id="btn-save"><strong>Simpan</strong></button>
                                        <button type="button" class="btn btn-danger" id="btn-batal"><strong>Batal</strong></button>
                                        <button type="submit" style="display:none" class="btn btn-info" id="validasi"><strong>validasi</strong></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

        <div class="copy hide">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="number" name="petik_penelitian[]" class="form-control" placeholder="Kutipan">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
            </div>
          </div>
        </div>
    </div>


<script type="text/javascript">
    $(document).ready(function() {
      $(".add-more").click(function(){ 
          var html = $(".copy").html();
          $(".after-add-more").after(html);
      });
      $("body").on("click",".remove",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>