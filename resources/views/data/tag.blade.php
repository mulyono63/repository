<div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 id="card-title">Daftar Data TAG</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="float-right">
                  @foreach ($errors->all() as $error)
                 <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
                        <button id="btn-tambah" class="btn btn-primary btn-sm"><strong><i class="icon dripicons-plus" style="color: white;"></i>Tambah Data TAG</strong></button>
                        @if ($message = Session::get('succes'))
                            <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ $message }}</strong>
                            </div>
                        @elseif ($message = Session::get('delete'))
                            <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                  </div>
                    <div id="body-table">
                    <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tag</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php $a=0; ?> 
                               @foreach($d as $d)
                               <?php $a++; ?>
                            <tr>
									<td nowrap>{{$a}}</td>
									<td>{{$d->tag}}</td>
                                    <td>
                                        <button class="btn btn-success btn-sm" onclick="editBarang(`{{$d->id}}`,`{{$d->tag}}`)"><strong>Edit</strong></button>
                                        <button class="btn btn-danger btn-sm" onclick="hapusBarang(`{{$d->id}}`)"><strong>Hapus</strong></button>
                                    </td>
									
								            </tr>
                            @endforeach
                            
                              
                            </tbody>
                    </table>
                  </div>
                  <div id="body-form" style="display: none">
                        <form method="POST" action="posttag" class="form-horizontal" id="form-tambah"  enctype="multipart/form-data">
                        {{csrf_field()}}
                            <input readonly hidden type="text" name="flag_edit" id="flag_edit">
                            <input id="id"  type="hidden" name="id" class="form-control" value="{{ old('id') }}" required>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">TAG</label>
                                    <div class="col-md-5">
                                        <input id="tag" type="text" name="tag" placeholder="TAG"class="form-control" value="{{ old('tag') }}" required>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3"></label>
                                    <div class="col-md-5">
                                        <button type="submit" class="btn btn-info" id="btn-save"><strong>Simpan</strong></button>
                                        <button type="button" class="btn btn-danger" id="btn-batal"><strong>Batal</strong></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>