<div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 id="card-title">Daftar Account Dosen</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="float-right">
                  @foreach ($errors->all() as $error)
                 <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
                        <button id="btn-tambah" class="btn btn-primary btn-sm"><strong><i class="icon dripicons-plus" style="color: white;"></i>Tambah Account</strong></button>
                  </div>
                    <div id="body-table">
                    <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Nip</th>
                                    <th>Pendidikan</th>
                                    <th>email</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $a=0; ?>
                            @foreach($d as $d)
                            <?php $a++; ?>
                            <tr>
									<td nowrap>{{$a}}</td>
									<td>{{$d->nama}}, {{$d->id}}</td>
									<td>{{$d->nip}}</td>
									<td>{{$d->pendidikan}}</td>
									<td>{{$d->email}}</td>
                                    <?php if($d->activ==1){?>
									<td><h4><span class="label label-success">activ</span></h4></td>
                                   <?php }
                                    else{?>
									<td> <h4> <span class="label label-danger">Tidak Activ </span></h4></td>
                                   <?php } ?>

									<td>
                                    
                                    <?php if($d->activ==1){ ?>
                                        <form method="POST" class="form-horizontal" action="activdsn">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$d->nip}}">
                                        <input type="hidden" name="activ" value="0">
                                        <button class="btn btn-danger btn-sm" type="submit")><strong>OFF</strong></button>
                                        </form>
                                    <?php }else{ ?>
                                        <form method="POST" class="form-horizontal" action="activdsn">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$d->nip}}">
                                        <input type="hidden" name="activ" value="1">
                                        <button class="btn btn-success btn-sm" type="submit")><strong>ON</strong></button>
                                        </form>                                  
                                    <?php  } ?>
                                    

                                    </td>
									
								</tr>
                            @endforeach
                            
                              
                            </tbody>
                    </table>
                  </div>
                <div id="body-form" style="display: none">
                        <form method="POST" action="postdosen" class="form-horizontal" id="form-tambah"  enctype="multipart/form-data">
                        {{csrf_field()}}
                            <input readonly hidden type="text" name="flag_edit" id="flag_edit">
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Username</label>
                                    <div class="col-md-5">
                                        <input id="name" type="text" name="name" placeholder="username"class="form-control" value="{{ old('name') }}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Nama</label>
                                    <div class="col-md-5">
                                        <input id="nama" type="text" name="nama" placeholder="Nama"class="form-control" value="{{ old('nama') }}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Agama</label>
                                    <div class="col-md-5">
                                       <select name="agama" id="agama" class="form-control" required>
                                           <option value="">-PILIH-</option>
                                            <option value="Islam">Islam</option>
                                            <option value="Kristen">Kristen</option>
                                            <option value="Khatolik">Khatolik</option>
                                            <option value="Hindu">Hindu</option>
                                            <option value="Buda">Buda</option>
                                       </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Jenis Kelamin</label>
                                    <div class="col-md-5">
                                       <select name="jk" id="jk" class="form-control" required>
                                           <option value="">-PILIH-</option>
                                            <option value="L">Laki-Laki</option>
                                            <option value="P">Perempuan</option>
                                       </select>
                                    </div>
                                </div>
                            </div>


                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Gelar</label>
                                    <div class="col-md-5">
                                        <input id="gelar" type="text" name="gelar" placeholder="Gelar"class="form-control" value="{{ old('gelar') }}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">NIP</label>
                                    <div class="col-md-5">
                                        <input id="nip" type="number" name="nip" placeholder="nip"class="form-control" value="{{ old('npm') }}" required>
                                        <small style="color: red; display: none;" id="att-nip">Kode barang sudah digunakan</small>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">email</label>
                                    <div class="col-md-5">
                                        <input id="email" type="email" name="email" placeholder="email"class="form-control" value="{{ old('email') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Password</label>
                                    <div class="col-md-5">
                                        <input id="password" type="password" name="password" placeholder="password"class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Password Confirmation </label>
                                    <div class="col-md-5">
                                        <input id="confirmation" type="password" name="confirmation" placeholder="password"class="form-control" required >
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3"></label>
                                    <div class="col-md-5">
                                        <button type="submit" class="btn btn-info" id="btn-save"><strong>Simpan</strong></button>
                                        <button type="button" class="btn btn-danger" id="btn-batal"><strong>Batal</strong></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>