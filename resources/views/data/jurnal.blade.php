<div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 id="card-title">Daftar Data Jurnal</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="float-right">
                  @foreach ($errors->all() as $error)
                 <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
                @if(Auth::user()->role=='admin')
                @elseif(Auth::user()->role=='dosen')
                @else
                        <button id="btn-tambah" class="btn btn-primary btn-sm"><strong><i class="icon dripicons-plus" style="color: white;"></i>Tambah Data Jurnal</strong></button>
                @endif
                  </div>
                    <div id="body-table">
                    <table id="datatable" class="table table-striped table-bordered">
                    <thead bgcolor="black">
                                <tr style="color:#FFFFFF">
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>Halaman</th>
                                    <th>Pengarang</th>
                                    <th>Dosen Pembimbing 1 dan 2</th>
                                    <th> <h4><i class="fa fa-eye"></i></h4> </th>
                                    <th><h4><i class="fa fa-cloud-download"></i></h4></th>
                                    <th>Kutipan</th>
                                    <th>Tag</th>
                                    <th>Status</th>
                                    <th>File</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $a=0; ?>
                            @foreach($d as $d)
                            <?php $a++; ?>
                            <tr>
                                    <td nowrap>{{$a}}</td>
                                    <td>{{$d->judul}}</td>
                                    <td>{{$d->halaman}}</td>
                                    <td>{{$d->pengarang_1}} /</br> {{$d->pengarang_2}} /</br> {{$d->pengarang_3}}</td>
                                    <td>{{$d->pembimbing_1}} /</br> {{$d->pembimbing_2}}</td>
                                    <td>{{$d->view_jurnal}}</td>
                                    <td>{{$d->download_jurnal}}</td>
                                    <td>{{$d->petik_jurnal}}</td>
                                    <td>{{$d->tag}}</td>
                                    <td>
                                    @if($d->active_j==1)
                                    <h2><span class="label label-success">Berhasil</span></h2>
                                    @elseif($d->active_j==0)
                                    <h2><span class="label label-warning">Menungu...</span></h2>
                                    @else
                                    <h2><span class="label label-danger">Ditolak...</span></h2>
                                    @endif
                                    </td>
                                    @if($d->active_j==1)
                                    <td><a href="{{asset('data_file/'.$d->file)}}"><img  src="{{asset('img/userimg/file.png')}}"></a></td>
                                    @elseif($d->active_j==0)
                                    <td><a href="{{asset('data_file/'.$d->file)}}"><img  src="{{asset('img/userimg/file.png')}}"></a></td>
                                    @else
                                    <td></td>
                                    @endif
                                    <td>
                                    @if(Auth::user()->role=='dosen')
                                    @if($d->active_j==1)
                                    @elseif($d->active_j==2)
                                    @else
                                    <button class="btn btn-success btn-sm" onclick="proses(`{{$d->id}}`,`{{$d->judul}}`,`{{$d->abstract}}`,`{{$d->pengarang_1}}`,`{{$d->pengarang_2}}`,`{{$d->pengarang_3}}`,`{{$d->id_tag}}`,`{{$d->halaman}}`,`{{$d->pembimbing_1}}`,`{{$d->pembimbing_2}}`,`{{$d->file}}`,`{{$d->petik_jurnal}}`)"><strong>Validasi</strong></button>
                                    @endif
                                    @else
                                    @if(Auth::user()->role =='admin')
                                    
                                    
                                        
                                        @if($d->active_j==1)
                                        <a href="deletejurnal/{{$d->id}}/{{$d->file}}" class="btn btn-danger btn-sm">Hapus</a>
                                        @elseif($d->active_j==0)
                                        <a href="deletejurnal/{{$d->id}}/{{$d->file}}" class="btn btn-danger btn-sm">Hapus</a>
                                        @else
                                        <a href="deletejurnal/{{$d->id}}/{{$d->file}}" class="btn btn-danger btn-sm">Hapus</a>
                                        @endif 
                                    @else
                                    <button class="btn btn-success btn-sm" onclick="editskripsi(`{{$d->id}}`,`{{$d->judul}}`,`{{$d->abstract}}`,`{{$d->pengarang_1}}`,`{{$d->pengarang_2}}`,`{{$d->pengarang_3}}`,`{{$d->halaman}}`,`{{$d->pembimbing_1}}`,`{{$d->pembimbing_2}}`,`{{$d->file}}`)"><strong>Edit</strong></button>
                                    
                                        
                                        @if($d->active_j==1)
                                        <a href="deletejurnal/{{$d->id}}/{{$d->file}}" class="btn btn-danger btn-sm">Hapus</a>
                                        @elseif($d->active_j==0)
                                        <a href="deletejurnal/{{$d->id}}/{{$d->file}}" class="btn btn-danger btn-sm">Hapus</a>
                                        @else
                                        <a href="deletejurnal/{{$d->id}}/{{$d->file}}" class="btn btn-danger btn-sm">Hapus</a>
                                        @endif                                    
                                    @endif
                                    @endif
                                    </td>
                                    
                                </tr>
                            @endforeach
                            </tbody>
                    </table>
                  </div>
                  <div id="body-form" style="display: none">
                        <form method="POST" action="postjurnal" class="form-horizontal" id="form-tambah"  enctype="multipart/form-data">
                        {{csrf_field()}}
                            <input readonly hidden type="text" name="flag_edit" id="flag_edit">
                            <input readonly hidden type="text" name="file_del" id="file">
                            <input readonly hidden type="text" name="id" id="id">
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Judul Jurnal</label>
                                    <div class="col-md-5">
                                        <input id="judul" type="text" name="judul" placeholder="Judul Jurnal"class="form-control" value="{{ old('judul') }}" maxlength="200" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Abstract</label>
                                    <div class="col-md-5">
                                    <textarea class="form-control" rows="3" name="abstract" id="abstract" value="{{old('abstract')}}" maxlength="400" placeholder="Abstract Jurnal" required=""></textarea>                                    
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Pengarang 1</label>
                                    <div class="col-md-5">
                                        <input id="pengarang_1" type="text" name="pengarang_1" placeholder="Pengarang"class="form-control" value="{{ old('pengarang_1') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Pengarang 2</label>
                                    <div class="col-md-5">
                                        <input id="pengarang_2" type="text" name="pengarang_2" placeholder="Pengarang"class="form-control" value="{{ old('pengarang_2') }}" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Pengarang 3</label>
                                    <div class="col-md-5">
                                        <input id="pengarang_3" type="text" name="pengarang_3" placeholder="Pengarang"class="form-control" value="{{ old('pengarang_3') }}" >
                                    </div>
                                </div>
                            </div>

                            <div  class="form-body">
                                <div id="id_taga" class="form-group row">
                                    <label class="control-label text-right col-md-3">TAG</label>
                                    <div class="col-md-5">
                                    <div class="input-group control-group after-add-morea">
                                      <select name="id_tag[]" id="id_tag" class="form-control" required>
                                        <option value="" >-PILIH-</option>
                                           @foreach($c as $a)
                                            <option value="{{$a->id}}">{{$a->tag}}</option>
                                           @endforeach
                                       </select>
                                      <div class="input-group-btn"> 
                                        <button class="btn btn-success add-morea" type="button"><i class="glyphicon glyphicon-plusa"></i> Tambah</button>
                                      </div>
                                    </div>                                        
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Halaman</label>
                                    <div class="col-md-5">
                                        <input id="halaman" type="number" name="halaman" placeholder="Halaman"class="form-control" value="{{ old('halaman') }}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Dosen Pembimbing 1</label>
                                    <div class="col-md-5">
                                       <select name="pembimbing_1" id="pembimbing_1" class="form-control" required>
                                           <option value="">-PILIH-</option>
                                           @foreach($b as $a)
                                            <option value="{{$a->nip}}">{{$a->nama}}, {{$a->gelar}}</option>
                                           @endforeach
                                       </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Dosen Pembimbing 2</label>
                                    <div class="col-md-5">
                                       <select name="pembimbing_2" id="pembimbing_2" class="form-control">
                                           <option value="">-PILIH-</option>
                                           @foreach($b as $a)
                                            <option value="{{$a->nip}}">{{$a->nama}}, {{$a->gelar}}</option>
                                           @endforeach
                                       </select>
                                    </div>
                                </div>
                            </div>

                            <div  class="form-body" id="kut">
                                <div id="petik_jurnala" class="form-group row">
                                    <label class="control-label text-right col-md-3">Kutipan</label>
                                    <div class="col-md-5">
                                    <div class="input-group control-group after-add-more">
                                      <input type="number" name="petik_jurnal[]"  id="petik_jurnal" class="form-control" placeholder="Kutipan" required>
                                      <div class="input-group-btn"> 
                                        <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Tambah</button>
                                      </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body" id="fil">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">File Jurnal</label>
                                    <div class="col-md-5">
                                        <input id="file" type="file" value="" name="file" class="form-control" >
                                        <span>Frotmat file pdf size 5 MB</span>
                                    </div>
                                </div>
                            </div>
                            @if(Auth::user()->role=='dosen')
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Status</label>
                                    <div class="col-md-5">
                                       <select name="status_j"  class="form-control" required >
                                           <option value="">-PILIH-</option>
                                            <option value="1">Lolos</option>
                                            <option value="2">Tidak Lolos</option>
                                       </select>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3"></label>
                                    <div class="col-md-5">
                                        <button type="submit" class="btn btn-info" id="btn-save"><strong>Simpan</strong></button>
                                        <button type="button" class="btn btn-danger" id="btn-batal"><strong>Batal</strong></button>
                                        <button type="submit" style="display:none" class="btn btn-info" id="validasi"><strong>validasi</strong></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="copy hide">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="number" name="petik_jurnal[]" class="form-control" placeholder="Kutipan">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
            </div>
          </div>
        </div>
    </div>

    <div class="copya hide">
          <div class="control-groupa input-group" style="margin-top:10px">
            <select name="id_tag[]" id="id_tag" class="form-control">
            <option value="" >-PILIH-</option>
            @foreach($c as $a)
            <option value="{{$a->id}}">{{$a->tag}}</option>
            @endforeach
            </select> 
            <div class="input-group-btn"> 
              <button class="btn btn-danger removea" type="button"><i class="glyphicon glyphicon-removea"></i> Hapus</button>
            </div>
          </div>
        </div>
    </div>


  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $(".add-more").click(function(){ 
          var html = $(".copy").html();
          $(".after-add-more").after(html);
      });
      $("body").on("click",".remove",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>


<script type="text/javascript">
    $(document).ready(function() {
      $(".add-morea").click(function(){ 
          var html = $(".copya").html();
          $(".after-add-morea").after(html);
      });
      $("body").on("click",".removea",function(){ 
          $(this).parents(".control-groupa").remove();
      });
    });
</script>