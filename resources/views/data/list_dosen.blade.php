<!DOCTYPE HTML>
<html>

<head>
  <title>REPOSITORY</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="{{asset('cs/main.css')}}" />
  <link rel="stylesheet" href="{{asset('cs/new.css')}}" />
  <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script> 
  @include('partials.cs_fron') 
<style type="text/css">
  .head{
  color:#1a2028;
  text-align: center;
}
.Tabs{
  padding-top: 50px;
  width:100%;
  height:300px;
}
.Tabs .TabMenu{
  height: 15%;
}
.Tabs .TabMenu button{
  width: 20%;
  height: 100%;
  float: left;
  border:none;
  outline: none;
  cursor: pointer;
  padding: 10px;
  font-size: 16px;
  font-weight: bold;
  background-color: #96ea2e;
}
.Tabs .TabMenu button:hover{
  background-color:#57db7a ;
}
.Tabs .tabPanel{
  height : 200%;
  width: 100%;
  background: #cccfff;
  text-align: center;
  padding-top: 50px;
  box-sizing: border-box;
  font-size: 18px;
  display: none;
}
</style>
</head>
<body class="is-preload">

  <!-- Wrapper -->
  <div id="wrapper">

    <!-- Main -->
    <div id="main">
      <div class="inner">

          <!-- Section -->
          <table style="margin-top: 50px;" id="datatable" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>NIK</th>
                <th>Dosen</th>
                <th>Jabatan</th>
                <th>Foto</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=0;
               foreach($d as $a){
                $no++; ?>
               <tr>
                 <th scope="row">{{$no}}</th>
                 <td><a href="/dosen/<?php echo $a->nip ?>">{{$a->nip}}</a></td>
                 <td>{{$a->nama}}, {{$a->gelar}}</td>
                 <td>{{$a->jabatan}}</td>
                 <td><img style="width: 80px; height: 80px;padding-bottom: 10px;" src="{{asset('img/userimg/'.$a->foto)}}"  alt=""  ></td>
               </tr>
               <?php } ?>
            </tbody>
          </table>
          <!-- Section -->

        </div>
      </div>

      <!-- Sidebar -->
      <div id="sidebar">
        <div class="inner">

          <!-- Menu -->
          @include('frontend.nav')

          <!-- Section -->
          

          <!-- Section -->
          @include('frontend.foter')
        </div>
      </div>

    </div>
        <script src="{{asset('js/browser.min.js')}}"></script>
    <script src="{{asset('js/breakpoints.min.js')}}"></script>
    <script src="{{asset('js/util.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script type="text/javascript">
          var tabButtons=document.querySelectorAll(".Tabs .TabMenu button");
    var tabPanels=document.querySelectorAll(".Tabs .tabPanel");

    function showPanel(panelIndex,colorCode){
      tabButtons.forEach(function(node){
        node.style.backgroundColor="";
        node.style.color="";
      });
      tabButtons[panelIndex].style.backgroundColor=colorCode;
      tabButtons[panelIndex].style.color="#662f00";
      tabPanels.forEach(function(node){
        node.style.display="none";
      });
      tabPanels[panelIndex].style.display="block";
      tabPanels[panelIndex].style.backgroundColor=colorCode;
    }
    </script>
  </body>
  </html>


    