<div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 id="card-title">Daftar Account Mahasiswa</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="float-right">
                  @foreach ($errors->all() as $error)
                 <div class="alert alert-danger">{{ $error }}</div>
                @endforeach
                        <!-- <button id="btn-tambah" class="btn btn-primary btn-sm"><strong><i class="icon dripicons-plus" style="color: white;"></i>Tambah Account</strong></button> -->
                  </div>
                    <div id="body-table">
                    <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Npm</th>
                                    <th>Pendidikan</th>
                                    <th>email</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $a=0; ?>
                            @foreach($d as $d)
                            <?php $a++; ?>
                            <tr>
									<td nowrap>{{$a}}</td>
									<td>{{$d->nama}}</td>
									<td>{{$d->npm}}</td>
									<td>{{$d->pendidikan}}</td>
									<td>{{$d->email}}</td>
                                    <?php if($d->activ==1){?>
									<td><h4><span class="label label-success">Activ</span></h4></td>
                                   <?php }
                                    else{?>
									<td><h4><span class="label label-danger">Tidak Activ</span></h4></td>
                                   <?php } ?>

									<td>
                                    <?php if($d->activ==1){ ?>
                                        <form method="POST" class="form-horizontal" action="activmhs">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$d->npm}}">
                                        <input type="hidden" name="activ" value="0">
                                        <button class="btn btn-danger btn-sm" type="submit")><strong>OFF</strong></button>
                                        </form>
                                    <?php }else{ ?>
                                        <form method="POST" class="form-horizontal" action="activmhs">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$d->npm}}">
                                        <input type="hidden" name="activ" value="1">
                                        <button class="btn btn-success btn-sm" type="submit")><strong>ON</strong></button>
                                        </form>                                  
                                    <?php  } ?>
                                    

                                    </td>
									
								</tr>
                            @endforeach
                            
                              
                            </tbody>
                    </table>
                  </div>
                  <div id="body-form" style="display: none">
                        <form method="POST" action="postdosen" class="form-horizontal" id="form-tambah"  enctype="multipart/form-data">
                        {{csrf_field()}}
                            <input readonly hidden type="text" name="flag_edit" id="flag_edit">
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Username</label>
                                    <div class="col-md-5">
                                        <input id="name" type="text" name="name" placeholder="username"class="form-control" value="{{ old('name') }}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">NIP</label>
                                    <div class="col-md-5">
                                        <input id="npm" type="number" name="npm" placeholder="nip"class="form-control" value="{{ old('npm') }}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">email</label>
                                    <div class="col-md-5">
                                        <input id="email" type="email" name="email" placeholder="email"class="form-control" value="{{ old('email') }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Password</label>
                                    <div class="col-md-5">
                                        <input id="password" type="password" name="password" placeholder="password"class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">Password Confirmation </label>
                                    <div class="col-md-5">
                                        <input id="confirmation" type="password" name="confirmation" placeholder="password"class="form-control" required >
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3"></label>
                                    <div class="col-md-5">
                                        <button type="submit" class="btn btn-info" id="btn-save"><strong>Simpan</strong></button>
                                        <button type="button" class="btn btn-danger" id="btn-batal"><strong>Batal</strong></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>