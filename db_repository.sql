-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2020 at 02:50 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coba`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tab_documen`
--

CREATE TABLE `tab_documen` (
  `id` int(11) NOT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `judul_file` varchar(50) DEFAULT NULL,
  `nama_file` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_documen`
--

INSERT INTO `tab_documen` (`id`, `id_dosen`, `judul_file`, `nama_file`, `created_at`, `updated_at`) VALUES
(4, 16753049, 'Tes', '1581832030_21.pdf', '2020-02-15 22:47:10', '2020-02-15 22:47:10'),
(5, 16753049, 'coba', '1582067500_21.pdf', '2020-02-18 16:11:40', '2020-02-18 16:11:40');

-- --------------------------------------------------------

--
-- Table structure for table `tab_dosen`
--

CREATE TABLE `tab_dosen` (
  `id` int(20) NOT NULL,
  `nip` int(20) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `gelar` varchar(20) DEFAULT NULL,
  `pendidikan` text,
  `agama` varchar(50) NOT NULL,
  `jk` varchar(50) NOT NULL,
  `nidn` int(11) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(200) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_dosen`
--

INSERT INTO `tab_dosen` (`id`, `nip`, `nama`, `gelar`, `pendidikan`, `agama`, `jk`, `nidn`, `jabatan`, `tempat_lahir`, `tgl_lahir`, `created_at`, `updated_at`) VALUES
(11, 16753049, 'ridho iqbal', 'Amd.P', 'Manajemen Infotrmatika', 'Islam', 'L', 3423453, 'Rektor', 'Sumberjo', '1997-07-10', '2020-02-15 19:40:46', '2020-02-16 05:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `tab_download`
--

CREATE TABLE `tab_download` (
  `id` int(11) NOT NULL,
  `id_penelitian` int(11) DEFAULT NULL,
  `id_jurnal` int(11) DEFAULT NULL,
  `id_skripsi` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tab_jurnal`
--

CREATE TABLE `tab_jurnal` (
  `id` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL,
  `id_user` varchar(50) NOT NULL,
  `pembimbing_1` varchar(100) NOT NULL,
  `pembimbing_2` varchar(100) DEFAULT NULL,
  `judul` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `abstract` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pengarang_1` varchar(100) NOT NULL,
  `pengarang_2` varchar(100) DEFAULT NULL,
  `pengarang_3` varchar(100) DEFAULT NULL,
  `halaman` int(11) NOT NULL,
  `file` text NOT NULL,
  `active_j` enum('1','2','0') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tab_mhs`
--

CREATE TABLE `tab_mhs` (
  `id` int(11) NOT NULL,
  `npm` varchar(50) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `gelar` text,
  `pendidikan` text,
  `agama` varchar(50) NOT NULL,
  `jk` varchar(50) NOT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_mhs`
--

INSERT INTO `tab_mhs` (`id`, `npm`, `nama`, `gelar`, `pendidikan`, `agama`, `jk`, `tempat_lahir`, `tgl_lahir`, `created_at`, `updated_at`) VALUES
(9, '324234', 'Mulyono', NULL, NULL, 'Islam', 'Laki-Laki', NULL, NULL, '2020-02-15 19:35:46', '2020-02-15 19:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `tab_pendidikan`
--

CREATE TABLE `tab_pendidikan` (
  `id` int(11) NOT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `perguruan_tinggi` varchar(100) NOT NULL,
  `gelar` varchar(25) NOT NULL,
  `tahun_ijasah` int(11) DEFAULT NULL,
  `jenjang` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_pendidikan`
--

INSERT INTO `tab_pendidikan` (`id`, `id_dosen`, `perguruan_tinggi`, `gelar`, `tahun_ijasah`, `jenjang`, `created_at`, `updated_at`) VALUES
(4, 16753049, 'Politeknik Negeri Lampung', 'Amd.Kom', 2019, 'D3', '2020-02-15 22:46:39', '2020-02-15 22:46:39');

-- --------------------------------------------------------

--
-- Table structure for table `tab_penelitian`
--

CREATE TABLE `tab_penelitian` (
  `id` int(11) NOT NULL,
  `id_user` varchar(50) NOT NULL,
  `pembimbing_1` varchar(100) NOT NULL,
  `pembimbing_2` varchar(100) DEFAULT NULL,
  `judul` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `abstract` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pengarang_1` varchar(100) NOT NULL,
  `pengarang_2` varchar(100) DEFAULT NULL,
  `pengarang_3` varchar(100) DEFAULT NULL,
  `halaman` int(11) NOT NULL,
  `file` text NOT NULL,
  `active_p` enum('1','2','0') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_penelitian`
--

INSERT INTO `tab_penelitian` (`id`, `id_user`, `pembimbing_1`, `pembimbing_2`, `judul`, `abstract`, `pengarang_1`, `pengarang_2`, `pengarang_3`, `halaman`, `file`, `active_p`, `created_at`, `updated_at`) VALUES
(451, '16753049', '16753049', NULL, 'Strategi Pemasaran Bisnis Kuliner Menggunakan Influencer  Melalui Media Sosial Instagram', 'Strategi Pemasaran Bisnis Kuliner Menggunakan Influencer  Melalui Media Sosial Instagram', 'Bagus', NULL, NULL, 38, '1581823467_21.pdf', '1', '2020-02-16 03:39:55', '2020-02-16 03:39:55');

-- --------------------------------------------------------

--
-- Table structure for table `tab_petik`
--

CREATE TABLE `tab_petik` (
  `id` int(11) NOT NULL,
  `id_penelitian` int(11) DEFAULT NULL,
  `id_skripsi` int(11) DEFAULT NULL,
  `id_jurnal` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_petik`
--

INSERT INTO `tab_petik` (`id`, `id_penelitian`, `id_skripsi`, `id_jurnal`, `created_at`, `updated_at`) VALUES
(3, NULL, 211, NULL, '2020-02-15 19:57:25', '2020-02-15 19:57:25'),
(3, NULL, NULL, 211, '2020-02-15 20:07:18', '2020-02-15 20:07:18'),
(4, 451, NULL, NULL, '2020-02-15 20:18:38', '2020-02-15 20:18:38'),
(211, NULL, 211, NULL, '2020-02-20 04:18:36', '2020-02-20 04:18:36');

-- --------------------------------------------------------

--
-- Table structure for table `tab_skripsi`
--

CREATE TABLE `tab_skripsi` (
  `id` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL,
  `id_user` varchar(50) NOT NULL,
  `pembimbing_1` varchar(100) NOT NULL,
  `pembimbing_2` varchar(100) DEFAULT NULL,
  `judul` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `abstract` text CHARACTER SET utf8 COLLATE utf8_bin,
  `pengarang_1` varchar(100) NOT NULL,
  `halaman` int(11) NOT NULL,
  `file` text NOT NULL,
  `active_s` enum('1','2','0') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_skripsi`
--

INSERT INTO `tab_skripsi` (`id`, `id_tag`, `id_user`, `pembimbing_1`, `pembimbing_2`, `judul`, `abstract`, `pengarang_1`, `halaman`, `file`, `active_s`, `created_at`, `updated_at`) VALUES
(211, 2, '324234', '16753049', NULL, 'APLIKASI POINT OF SALE BERBASIS WEB PADA TOKO BUKU XYZ', 'APLIKASI POINT OF SALE BERBASIS WEB PADA TOKO BUKU XYZ APLIKASI POINT OF SALE BERBASIS WEB PADA TOKO BUKU XYZ APLIKASI POINT OF SALE BERBASIS WEB PADA TOKO BUKU XYZ', 'Shalihah', 32, '1582197516_21.pdf', '1', '2020-02-20 11:31:33', '2020-02-20 11:31:33');

-- --------------------------------------------------------

--
-- Table structure for table `tab_tag`
--

CREATE TABLE `tab_tag` (
  `id` int(11) NOT NULL,
  `tag` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_tag`
--

INSERT INTO `tab_tag` (`id`, `tag`, `created_at`, `updated_at`) VALUES
(1, 'Android', NULL, NULL),
(2, 'Web', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tab_tag_detail`
--

CREATE TABLE `tab_tag_detail` (
  `id` int(11) NOT NULL,
  `id_penelitian` int(11) DEFAULT NULL,
  `id_skripsi` int(11) DEFAULT NULL,
  `id_jurnal` int(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_tag_detail`
--

INSERT INTO `tab_tag_detail` (`id`, `id_penelitian`, `id_skripsi`, `id_jurnal`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, 211, NULL, '1', '2020-02-15 19:57:25', '2020-02-20 11:27:52'),
(2, NULL, 211, NULL, '1', '2020-02-15 19:57:25', '2020-02-20 11:27:52'),
(2, NULL, NULL, 211, '0', '2020-02-15 20:07:18', '2020-02-15 20:07:18'),
(1, NULL, 211, NULL, '1', '2020-02-20 04:18:36', '2020-02-20 11:27:52'),
(2, NULL, 211, NULL, '1', '2020-02-20 04:18:36', '2020-02-20 11:27:52');

-- --------------------------------------------------------

--
-- Table structure for table `tab_view`
--

CREATE TABLE `tab_view` (
  `id` int(11) NOT NULL,
  `id_penelitian` int(11) DEFAULT NULL,
  `id_skripsi` int(11) DEFAULT NULL,
  `id_jurnal` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_view`
--

INSERT INTO `tab_view` (`id`, `id_penelitian`, `id_skripsi`, `id_jurnal`, `created_at`, `updated_at`) VALUES
(1, 451, NULL, NULL, '2020-02-18 07:05:05', '2020-02-18 07:05:05'),
(2, 451, NULL, NULL, '2020-02-18 07:12:41', '2020-02-18 07:12:41'),
(3, 451, NULL, NULL, '2020-02-18 07:12:52', '2020-02-18 07:12:52'),
(4, 451, NULL, NULL, '2020-02-18 07:20:22', '2020-02-18 07:20:22'),
(5, 451, NULL, NULL, '2020-02-18 07:23:42', '2020-02-18 07:23:42'),
(6, 451, NULL, NULL, '2020-02-18 07:24:02', '2020-02-18 07:24:02'),
(7, 451, NULL, NULL, '2020-02-18 07:24:44', '2020-02-18 07:24:44'),
(8, 451, NULL, NULL, '2020-02-18 07:48:29', '2020-02-18 07:48:29'),
(9, 451, NULL, NULL, '2020-02-18 07:48:53', '2020-02-18 07:48:53'),
(10, 451, NULL, NULL, '2020-02-18 07:49:05', '2020-02-18 07:49:05'),
(11, 451, NULL, NULL, '2020-02-18 07:49:18', '2020-02-18 07:49:18'),
(12, 451, NULL, NULL, '2020-02-18 07:54:02', '2020-02-18 07:54:02'),
(13, 451, NULL, NULL, '2020-02-18 07:57:14', '2020-02-18 07:57:14'),
(14, 451, NULL, NULL, '2020-02-18 07:58:30', '2020-02-18 07:58:30'),
(15, 451, NULL, NULL, '2020-02-18 08:00:01', '2020-02-18 08:00:01'),
(16, 451, NULL, NULL, '2020-02-18 08:15:44', '2020-02-18 08:15:44'),
(17, 451, NULL, NULL, '2020-02-18 08:23:05', '2020-02-18 08:23:05'),
(18, 451, NULL, NULL, '2020-02-18 08:23:26', '2020-02-18 08:23:26'),
(19, 451, NULL, NULL, '2020-02-18 08:23:49', '2020-02-18 08:23:49'),
(20, 451, NULL, NULL, '2020-02-18 08:24:07', '2020-02-18 08:24:07'),
(21, 451, NULL, NULL, '2020-02-18 08:24:14', '2020-02-18 08:24:14'),
(22, 451, NULL, NULL, '2020-02-18 08:26:07', '2020-02-18 08:26:07'),
(23, 451, NULL, NULL, '2020-02-18 08:26:37', '2020-02-18 08:26:37'),
(24, 451, NULL, NULL, '2020-02-18 08:29:08', '2020-02-18 08:29:08'),
(25, 451, NULL, NULL, '2020-02-18 08:40:47', '2020-02-18 08:40:47'),
(26, 451, NULL, NULL, '2020-02-18 08:41:41', '2020-02-18 08:41:41'),
(27, 451, NULL, NULL, '2020-02-18 08:42:04', '2020-02-18 08:42:04'),
(28, 451, NULL, NULL, '2020-02-18 08:42:39', '2020-02-18 08:42:39'),
(29, 451, NULL, NULL, '2020-02-18 08:43:11', '2020-02-18 08:43:11'),
(30, 451, NULL, NULL, '2020-02-18 08:44:19', '2020-02-18 08:44:19'),
(31, 451, NULL, NULL, '2020-02-18 08:45:15', '2020-02-18 08:45:15'),
(32, 451, NULL, NULL, '2020-02-18 15:39:47', '2020-02-18 15:39:47'),
(33, 451, NULL, NULL, '2020-02-18 15:42:21', '2020-02-18 15:42:21'),
(34, 451, NULL, NULL, '2020-02-18 15:44:16', '2020-02-18 15:44:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `npm` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user.png',
  `activ` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `npm`, `name`, `role`, `email`, `email_verified_at`, `password`, `remember_token`, `foto`, `activ`, `created_at`, `updated_at`) VALUES
('123654', '123654', 'admin', 'admin', 'admin@gmail.com', NULL, '$2y$10$ZKHESkTzfRn9qJ9iLTTo3Oe6gPhVbw27kkbc2f8N9IjrsGKxPAphK', '5f1flauOevO8eii2VOVjUVezvXKcV0XZk6mnaDLV3fZZ3cHTW9ZgvgtgMUVM', '1579735836_albanna 2.jpg', '1', NULL, NULL),
('16753049', '16753049', 'ridho iqbal', 'dosen', 'ridhoiqbal@gmail.com', NULL, '$2y$10$jwodWLbvau5uMmKF4Wogo.GzlkJ8SLDAUVnE4Qt9aVEj/ISFmcRNG', NULL, 'user.png', '1', '2020-02-15 19:40:46', '2020-02-15 19:40:46'),
('324234', '324234', 'Mulyono', 'mahasiswa', 'mulyonomuiz63@gmail.com', NULL, '$2y$10$6dKB3M4CSX4zGQFdsG43zuzb0OAi9pvF46Hf4/fEhdeU79iIYZRNu', NULL, 'user.png', '1', '2020-02-15 19:35:46', '2020-02-15 19:35:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tab_documen`
--
ALTER TABLE `tab_documen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tab_dosen`
--
ALTER TABLE `tab_dosen`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `npm` (`nip`);

--
-- Indexes for table `tab_download`
--
ALTER TABLE `tab_download`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tab_jurnal`
--
ALTER TABLE `tab_jurnal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tab_mhs`
--
ALTER TABLE `tab_mhs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `npm` (`npm`);

--
-- Indexes for table `tab_pendidikan`
--
ALTER TABLE `tab_pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tab_penelitian`
--
ALTER TABLE `tab_penelitian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tab_skripsi`
--
ALTER TABLE `tab_skripsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tab_tag`
--
ALTER TABLE `tab_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tab_view`
--
ALTER TABLE `tab_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tab_documen`
--
ALTER TABLE `tab_documen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tab_dosen`
--
ALTER TABLE `tab_dosen`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tab_download`
--
ALTER TABLE `tab_download`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tab_jurnal`
--
ALTER TABLE `tab_jurnal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;

--
-- AUTO_INCREMENT for table `tab_mhs`
--
ALTER TABLE `tab_mhs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tab_pendidikan`
--
ALTER TABLE `tab_pendidikan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tab_penelitian`
--
ALTER TABLE `tab_penelitian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=452;

--
-- AUTO_INCREMENT for table `tab_skripsi`
--
ALTER TABLE `tab_skripsi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;

--
-- AUTO_INCREMENT for table `tab_tag`
--
ALTER TABLE `tab_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tab_view`
--
ALTER TABLE `tab_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
