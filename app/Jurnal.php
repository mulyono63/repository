<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurnal extends Model
{
    protected $table="tab_jurnal";
    protected $fillable=['id','id_tag','id_user','pembimbing_1','pembimbing_2','judul','abstract', 'pengarang_1','pengarang_2','pengarang_3','halaman','file','active_j'];
}