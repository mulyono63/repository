<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table="tab_dosen";
    protected $fillable=['id','nip','nama', 'gelar','pendidikan','agama','jk','nidn','jabatan','tempat_lahir','tgl_lahir'];
}
