<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    protected $table="tab_pendidikan";
    protected $fillable=['id','id_dosen','gelar', 'perguruan_tinggi', 'tahun_ijasah','jenjang'];
}
