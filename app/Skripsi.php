<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skripsi extends Model
{
    protected $table="tab_skripsi";
    protected $fillable=['id','id_tag','id_user','pembimbing_1','pembimbing_2','judul','abstract', 'pengarang_1','halaman','file','active_s'];
}
