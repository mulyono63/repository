<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    protected $table="tab_download";
    protected $fillable=['id','id_penelitian','id_skripsi', 'id_jurnal'];
}
