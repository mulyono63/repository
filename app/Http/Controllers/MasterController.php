<?php

namespace App\Http\Controllers;

use App\Welcome;
use Auth;
use App\User;
use App\Dosen;
use App\Skripsi;
use App\Penelitian;
use App\Jurnal;
use App\Petik;
use App\Tag_detail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;


class MasterController extends Controller
{
    public function index()
    {      
    // dd($output);
    $data = array(
        'thisContent'   => 'dashboards.dashboard',
        'thisJs'        => 'Js.Jswelcome',
        'page1'         => 'Administrasi 1',
        'page2'         => 'Administrasi 2',
    );
    return view('layouts.ms')->with($data);
    }

    public function data_file($id)
    {
        $dec=Crypt::decrypt($id);
        if(DB::select("select * from tab_penelitian where id=$dec"))
        {
            $a=\App\Penelitian::find($dec);
            $b=$a->id;
            // dd($b);
            $ver=new \App\Download;
            $ver->id_penelitian= $b;
            $ver->save();
            return redirect(asset('data_file/'.$a->file));
        }elseif(DB::select("select * from tab_skripsi where id=$dec"))
        {
            $a=\App\Skripsi::find($dec);
            $b=$a->id;
            // dd($b);
            $ver=new \App\Download;
            $ver->id_skripsi= $b;
            $ver->save();
            return redirect(asset('data_file/'.$a->file));    
        }elseif(DB::select("select * from tab_jurnal where id=$dec"))
        {
            $a=\App\Jurnal::find($dec);
            $b=$a->id;
            // dd($b);
            $ver=new \App\Download;
            $ver->id_jurnal= $b;
            $ver->save();
            return redirect(asset('data_file/'.$a->file));
        }        
    }

    public function jurnal()
    {
        if(Auth::user()->role=='admin'){
            $c=DB::select('select * from tab_tag');
            $b=DB::select('select * from tab_dosen');
            // dd($b);
            $d=DB::select("select a.id, a.judul,a.halaman,a.active_j,a.pengarang_1,a.pengarang_2,a.pengarang_3,a.file, b.tag, 
                            (select nama from tab_dosen where nip = a.pembimbing_1) as pembimbing_1,
                            (select nama from tab_dosen where nip = a.pembimbing_2) as pembimbing_2, 
                            (select COUNT(tab_download.id_jurnal) from tab_download WHERE tab_download.id_jurnal=a.id ) as download_jurnal, 
                            (select COUNT(tab_view.id_jurnal) from tab_view WHERE tab_view.id_jurnal=a.id) as view_jurnal,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik_jurnal
                            from tab_jurnal a join tab_tag b on a.id_tag=b.id");
            $data =array(
                'thisContent'   => 'data.jurnal',
                'thisJs'        => 'Js.Jsjurnal',
                'page1'         => 'Adminitrasi 1',
                'page2'         => 'Administrasi 2',
            );
            return view('layouts.ms', compact('d','b','c'))->with($data);
        }elseif(Auth::user()->role=='dosen'){
            $c=DB::select('select * from tab_tag');
            $b=DB::select('select * from tab_dosen');
            $id_user=Auth::user()->npm;
            $d=DB::select("select a.*, b.tag, 
                            (select COUNT(tab_download.id_jurnal) from tab_download WHERE tab_download.id_jurnal=a.id ) as download_jurnal, 
                            (select COUNT(tab_view.id_jurnal) from tab_view WHERE tab_view.id_jurnal=a.id) as view_jurnal,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik_jurnal
                            from tab_jurnal a join tab_tag b on a.id_tag=b.id where a.pembimbing_1='$id_user' or a.pembimbing_2='$id_user'");
            // dd($id_user);
            $data =array(
                'thisContent'   => 'data.jurnal',
                'thisJs'        => 'Js.Jsjurnal',
                'page1'         => 'Adminitrasi 1',
                'page2'         => 'Administrasi 2',
            );
            return view('layouts.ms', compact('d','b','c'))->with($data);

        }else{
             $c=DB::select('select * from tab_tag');
            $b=DB::select('select * from tab_dosen');
            $id_user=Auth::user()->npm;
            $d=DB::select("select a.*, b.tag, 
                            (select COUNT(tab_download.id_jurnal) from tab_download WHERE tab_download.id_jurnal=a.id ) as download_jurnal, 
                            (select COUNT(tab_view.id_jurnal) from tab_view WHERE tab_view.id_jurnal=a.id) as view_jurnal,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik_jurnal
                            from tab_jurnal a join tab_tag b on a.id_tag=b.id where a.id_user='$id_user'");
            // dd($id_user);
            $data =array(
                'thisContent'   => 'data.jurnal',
                'thisJs'        => 'Js.Jsjurnal',
                'page1'         => 'Adminitrasi 1',
                'page2'         => 'Administrasi 2',
            );
            return view('layouts.ms', compact('d','b','c'))->with($data);
        }
    }

    public function postjurnal(request $request)
    {
        if($request->flag_edit==1)
        {
            File::delete('data_file/'.$request->file_del);
            
            $this->validate($request, [
                'file' => 'required|mimes:pdf|max:5048',
                'judul' => 'required|max:200',
                'abstract' => 'required|max:2000',
            ]);
    
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('file');
        
            $nama_file = time()."_".$file->getClientOriginalName();
     
              // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'data_file';
            $file->move($tujuan_upload,$nama_file);

            $a= DB::table('tab_jurnal')->where('id',$request->id)->update([
                'pembimbing_1' => $request['pembimbing_1'],
                'pembimbing_2' => $request['pembimbing_2'],
                'judul' => $request['judul'],
                'abstract' => $request['abstract'],
                'pengarang_1' => $request['pengarang_1'],
                'pengarang_2' => $request['pengarang_2'],
                'pengarang_3' => $request['pengarang_3'],
                'halaman' => $request['halaman'],
                'file' =>$nama_file,
                'active_j' =>'0',
            ]);
            return redirect()->back();
        }elseif($request->flag_edit==2){
            if($request->status_j==1){
                $a= DB::table('tab_jurnal')->where('id',$request->id)->update([
                'active_j' => $request['status_j'],
                ]);
                $b= DB::table('tab_tag_detail')->where('id_jurnal',$request->id)->update([
                'status' => '1',
                ]); 
            return redirect()->back();
            }else{
            File::delete('data_file/'.$request->file_del);
            $a= DB::table('tab_jurnal')->where('id',$request->id)->update([
                'active_j' => $request['status_j'],
            ]); 
            return redirect()->back();
            }
        }else{

            $this->validate($request, [
                'file' => 'required|file|mimes:pdf|max:5048',
                'judul' => 'required|max:200',
            ]);

            $noUrutAkhir = \App\Jurnal::max('id');
            $id_otomatis = (int)sprintf("%04s", abs($noUrutAkhir + 1));
            foreach ($request['id_tag'] as $data) {
            Tag_detail::create([
                'id' => $data,
                'id_jurnal' => "0021$id_otomatis",
            ]);
            }

            foreach ($request['petik_jurnal'] as $dataa) {
            Petik::create([
                'id' => $dataa,
                'id_jurnal' => "0021$id_otomatis",
            ]);
            }

    
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('file');
        
            $nama_file = time()."_".$file->getClientOriginalName();
     
              // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'data_file';
            $file->move($tujuan_upload,$nama_file);
    
            Jurnal::create([
                'id' => "0021$id_otomatis",
                'id_tag' => $data,
                'id_user' => Auth::user()->npm,
                'pembimbing_1' => $request['pembimbing_1'],
                'pembimbing_2' => $request['pembimbing_2'],
                'judul' => $request['judul'],
                'abstract' => $request['abstract'],
                'pengarang_1' => $request['pengarang_1'],
                'pengarang_2' => $request['pengarang_2'],
                'pengarang_3' => $request['pengarang_3'],
                'halaman' => $request['halaman'],
                'file' =>$nama_file,
            ]);
            
            return redirect()->back();
        }
    }

    public function deletejurnal($id, $file)
    {
        // dd($file);
        File::delete("data_file/".$file);
        $data = \App\Jurnal::find($id);
        $data->delete($data);
        return redirect()->back();
    }

    public function penelitian()
    {
        if(Auth::user()->role=='admin'){
            $b=DB::select('select * from tab_dosen');
            // dd($b);
            $d=DB::select("select a.*, 
                            (select COUNT(tab_download.id_penelitian) from tab_download WHERE tab_download.id_penelitian=a.id ) as download_penelitian, 
                            (select COUNT(tab_view.id_penelitian) from tab_view WHERE tab_view.id_penelitian=a.id) as view_penelitian,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik_penelitian
                            from tab_penelitian a");
            $data =array(
                'thisContent'   => 'data.penelitian',
                'thisJs'        => 'Js.Jspenelitian',
                'page1'         => 'Adminitrasi 1',
                'page2'         => 'Administrasi 2',
            );
            return view('layouts.ms', compact('d','b'))->with($data);
        }else{
            $b=DB::select('select * from tab_dosen');
            $id_user=Auth::user()->npm;
            // dd($id_user);
            $d=DB::select("select a.*, 
                            (select COUNT(tab_download.id_penelitian) from tab_download WHERE tab_download.id_penelitian=a.id ) as download_penelitian, 
                            (select COUNT(tab_view.id_penelitian) from tab_view WHERE tab_view.id_penelitian=a.id) as view_penelitian,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik_penelitian
                            from tab_penelitian a where a.id_user='$id_user'");
            $data =array(
                'thisContent'   => 'data.penelitian',
                'thisJs'        => 'Js.Jspenelitian',
                'page1'         => 'Adminitrasi 1',
                'page2'         => 'Administrasi 2',
            );
            return view('layouts.ms', compact('d','b'))->with($data);

        }
    }

    public function postpenelitian(request $request)
    {
        if($request->flag_edit==1)
        {
            File::delete('data_file/'.$request->file_del);
            
            $this->validate($request, [
                'file' => 'required|mimes:pdf|max:5048',
                'judul' => 'required|max:200',
                'abstract' => 'required|max:2000',
            ]);
    
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('file');
        
            $nama_file = time()."_".$file->getClientOriginalName();
     
              // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'data_file';
            $file->move($tujuan_upload,$nama_file);

            $a= DB::table('tab_penelitian')->where('id',$request->id)->update([
                'pembimbing_1' => $request['pembimbing_1'],
                'pembimbing_2' => $request['pembimbing_2'],
                'judul' => $request['judul'],
                'abstract' => $request['abstract'],
                'pengarang_1' => $request['pengarang_1'],
                'pengarang_2' => $request['pengarang_2'],
                'pengarang_3' => $request['pengarang_3'],
                'halaman' => $request['halaman'],
                'file' =>$nama_file,
                'active_p' =>'0',
            ]);
            return redirect()->back();
        }elseif($request->flag_edit==2){
            if($request['status_p'] == 1){
                $a= DB::table('tab_penelitian')->where('id',$request->id)->update([
                'active_p' => $request['status_p'],
                ]);
                $b= DB::table('tab_tag_detail')->where('id_penelitian',$request->id)->update([
                'status' => '1',
                ]); 
            return redirect()->back();
            }else{
                File::delete('data_file/'.$request->file_del);
                $a= DB::table('tab_penelitian')->where('id',$request->id)->update([
                'active_p' => $request['status_p'],
            ]); 
            return redirect()->back();
            }
        }else{
            $this->validate($request, [
                'file' => 'required|file|mimes:pdf|max:5048',
                'judul' => 'required|max:200',
                'abstract' => 'required|max:2000',
            ]);

            $noUrutAkhir = \App\Penelitian::max('id');
            $id_otomatis = (int)sprintf("%04s", abs($noUrutAkhir + 1));

            foreach ($request['petik_penelitian'] as $data) {
            Petik::create([
                'id' => $data,
                'id_penelitian' => "45$id_otomatis",
            ]);
            }
    
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('file');
        
            $nama_file = time()."_".$file->getClientOriginalName();
     
              // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'data_file';
            $file->move($tujuan_upload,$nama_file);
            
            Penelitian::create([
                'id' => "45$id_otomatis",
                'id_user' => Auth::user()->npm,
                'pembimbing_1' => $request['pembimbing_1'],
                'pembimbing_2' => $request['pembimbing_2'],
                'judul' => $request['judul'],
                'abstract' => $request['abstract'],
                'pengarang_1' => $request['pengarang_1'],
                'pengarang_2' => $request['pengarang_2'],
                'pengarang_3' => $request['pengarang_3'],
                'halaman' => $request['halaman'],
                'file' =>$nama_file,
            ]);
    
            return redirect()->back();
        }
    }

    public function deletepenelitian($id, $file)
    {
        // dd($file);
        File::delete("data_file/".$file);
        $data = \App\Penelitian::find($id);
        $data->delete($data);
        return redirect()->back();
    }


    public function skripsi()
    {
        if(Auth::user()->role=='admin'){

            $c=DB::select('select * from tab_tag');
            $b=DB::select('select * from tab_dosen');
            // dd($b);
            $d=DB::select('select a.id, a.judul,a.halaman,a.active_s,a.pengarang_1,a.file, b.tag, 
                            (select nama from tab_dosen where nip = a.pembimbing_1) as pembimbing_1,
                            (select nama from tab_dosen where nip = a.pembimbing_2) as pembimbing_2,
                            (select COUNT(tab_download.id_skripsi) from tab_download WHERE tab_download.id_skripsi=a.id ) as download_skripsi, 
                            (select COUNT(tab_view.id_skripsi) from tab_view WHERE tab_view.id_skripsi=a.id) as view_skripsi,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik_skripsi
                            from tab_skripsi a join tab_tag b on a.id_tag=b.id');
            $data =array(
                'thisContent'   => 'data.skripsi',
                'thisJs'        => 'Js.Jsskripsi',
                'page1'         => 'Adminitrasi 1',
                'page2'         => 'Administrasi 2',
            );
            return view('layouts.ms', compact('d','c','b'))->with($data);
        }elseif(Auth::user()->role=='dosen'){
            $c=DB::select('select * from tab_tag');
            $b=DB::select('select * from tab_dosen');
            $id_user=Auth::user()->npm;
            $d=DB::select("select a.*, b.tag, 
                            (select COUNT(tab_download.id_skripsi) from tab_download WHERE tab_download.id_skripsi=a.id ) as download_skripsi, 
                            (select COUNT(tab_view.id_skripsi) from tab_view WHERE tab_view.id_skripsi=a.id) as view_skripsi,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik_skripsi
                            from tab_skripsi a join tab_tag b on a.id_tag=b.id where a.pembimbing_1='$id_user' or a.pembimbing_2='$id_user'");
            // dd($d);
            $data =array(
                'thisContent'   => 'data.skripsi',
                'thisJs'        => 'Js.Jsskripsi',
                'page1'         => 'Adminitrasi 1',
                'page2'         => 'Administrasi 2',
            );
            return view('layouts.ms', compact('d','c','b'))->with($data);
        }else{
             $c=DB::select('select * from tab_tag');
            $b=DB::select('select * from tab_dosen');
            $id_user=Auth::user()->npm;
            $d=DB::select("select a.*, b.tag, 
                            (select COUNT(tab_download.id_skripsi) from tab_download WHERE tab_download.id_skripsi=a.id ) as download_skripsi, 
                            (select COUNT(tab_view.id_skripsi) from tab_view WHERE tab_view.id_skripsi=a.id) as view_skripsi,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik_skripsi
                            from tab_skripsi a join tab_tag b on a.id_tag=b.id where a.id_user = '$id_user'");
            // dd($d);
            $data =array(
                'thisContent'   => 'data.skripsi',
                'thisJs'        => 'Js.Jsskripsi',
                'page1'         => 'Adminitrasi 1',
                'page2'         => 'Administrasi 2',
            );
            return view('layouts.ms', compact('d','c','b'))->with($data);
        }
    }

    public function postskripsi(request $request)
    {
        if($request->flag_edit==1)
        {
            File::delete('data_file/'.$request->file_del);
            
            $this->validate($request, [
                'file' => 'required|mimes:pdf|max:5048',
            ]);
    
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('file');
        
            $nama_file = time()."_".$file->getClientOriginalName();
     
              // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'data_file';
            $file->move($tujuan_upload,$nama_file);

            $a= DB::table('tab_skripsi')->where('id',$request->id)->update([
                'pembimbing_1' => $request['pembimbing_1'],
                'pembimbing_2' => $request['pembimbing_2'],
                'judul' => $request['judul'],
                'abstract' => $request['abstract'],
                'pengarang_1' => $request['pengarang_1'],
                'halaman' => $request['halaman'],
                'file' =>$nama_file,
                'active_s' =>'0',
            ]);
            return redirect()->back();
        }elseif($request->flag_edit==2){
            if($request->status_s==1){
                $a= DB::table('tab_skripsi')->where('id',$request->id)->update([
                    'active_s' => $request['status_s'],
                ]); 
                $b= DB::table('tab_tag_detail')->where('id_skripsi',$request->id)->update([
                'status' => '1',
                ]);
                return redirect()->back();    
            }else{
            File::delete('data_file/'.$request->file_del);
            $a= DB::table('tab_skripsi')->where('id',$request->id)->update([
                'active_s' => $request['status_s'],
            ]); 
            return redirect()->back();
            }
        }else{
            $this->validate($request, [
                'file' => 'required|file|mimes:pdf|max:5048',
                'judul' => 'required|max:200',
                'abstract' => 'required|max:2000',
            ]);

            $noUrutAkhir = \App\Skripsi::max('id');
            $id_otomatis = (int)sprintf("%04s", abs($noUrutAkhir + 1));
            foreach ($request['id_tag'] as $data) {
            Tag_detail::create([
                'id' => $data,
                'id_skripsi' => "021$id_otomatis",
            ]);
            }
            foreach ($request['petik_skripsi'] as $dataa) {
            Petik::create([
               'id' => $dataa,
                'id_skripsi' => "021$id_otomatis",
            ]);
            }

    
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('file');
        
            $nama_file = time()."_".$file->getClientOriginalName();
     
              // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'data_file';
            $file->move($tujuan_upload,$nama_file);
    
            Skripsi::create([
                'id' => "021$id_otomatis",
                'id_tag' => $data,
                'id_user' => Auth::user()->npm,
                'pembimbing_1' => $request['pembimbing_1'],
                'pembimbing_2' => $request['pembimbing_2'],
                'judul' => $request['judul'],
                'abstract' => $request['abstract'],
                'pengarang_1' => $request['pengarang_1'],
                'halaman' => $request['halaman'],
                'file' =>$nama_file,
            ]);

            
    
            return redirect()->back();
        }
    }

    public function deleteskripsi($id, $file)
    {
        // dd($file);
        File::delete("data_file/".$file);
        $data = \App\Skripsi::find($id);
        $data->delete($data);
        return redirect()->back();
    }

    public function dosen()
    {
        $d=DB::select('select a.* , b.* from users a join tab_dosen b on a.id=b.nip');
        $data =array(
            'thisContent'   => 'data.dosen',
            'thisJs'        => 'Js.Jsdosen',
            'page1'         => 'Adminitrasi 1',
            'page2'         => 'Administrasi 2',
        );
        return view('layouts.ms', compact('d'))->with($data);
    }

    public function postdosen(request $request)
    {
        // dd($request->email);
        $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'confirmation' => 'required|same:password',
        ]);

        User::create([
            'id' => $request['nip'],
            'npm' => $request['nip'],
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'role' => 'dosen',
            'foto' => 'user.png',
        ]);

        Dosen::create([
            'nip'   => $request['nip'],
            'nama'   => $request['nama'],
            'gelar'   => $request['gelar'],
            'agama'   => $request['agama'],
            'jk'   => $request['jk'],
        ]);
        return redirect('/dosen');
    
    }

    //activ dosen
    public function activdsn(request $request)
    {
        
        // dd($request->activ);
       $a= DB::table('users')->where('id',$request->id)->update([
            'activ' => $request->activ
        ]);
        return redirect('/dosen');
    }

    //activ mahasiswa
    public function activmhs(request $request)
    {
        
        // dd($request->activ);
       $a= DB::table('users')->where('id',$request->id)->update([
            'activ' => $request->activ
        ]);
        return redirect('/mahasiswa');
    }

    public function mahasiswa()
    {
        $d=DB::select('select a.* , b.* from users a join tab_mhs b on a.id=b.npm');
        $data =array(
            'thisContent'   => 'data.mahasiswa',
            'thisJs'        => 'Js.Jsmahasiswa',
            'page1'         => 'Adminitrasi 1',
            'page2'         => 'Administrasi 2',
        );
        return view('layouts.ms', compact('d'))->with($data);
    }

    public function tag()
    {
        $d=\App\Tag::all();
        $data =array(
            'thisContent'   => 'data.tag',
            'thisJs'        => 'Js.Jstag',
            'page1'         => 'Adminitrasi 1',
            'page2'         => 'Administrasi 2',
        );
        return view('layouts.ms', compact('d'))->with($data);
    }

    public function posttag(request $request)
    {
        if($request->flag_edit==1)
        {
            $data = \App\Tag::find($request->id);
            $data->update($request->all());
        return redirect('tag')->with('succes','Data Berhasil Di Update');
        }else{
        \App\Tag::create($request->all());
        return redirect('tag')->with('succes','Data Berhasil Di Tambahkan');
        }
    }

    public function deletetag($id)
    {
        $data = \App\Tag::find($id);
        $data->delete($data);
        return redirect('tag')->with('delete','Data Berhasil Di Hapus');
    }

   public function statistik()
    {
        $d=DB::select("SELECT  COUNT(IF(EXTRACT(YEAR_MONTH FROM b.created_at) = EXTRACT(YEAR_MONTH FROM date(now())) ,1,NULL)) AS month, COUNT(IF(b.id_skripsi and EXTRACT(YEAR_MONTH FROM b.created_at) = EXTRACT(YEAR_MONTH FROM date(now())) ,1,NULL)) as total_month_skripsi, COUNT(IF(b.id_jurnal and EXTRACT(YEAR_MONTH FROM b.created_at) = EXTRACT(YEAR_MONTH FROM date(now())),1,NULL)) as total_month_jurnal, a.tag FROM tab_tag a JOIN tab_tag_detail b on a.id=b.id where b.status = '1'  group by a.tag ");

        $a=DB::select("SELECT COUNT(IF(b.id,1,NULL)) AS total, COUNT(IF(b.id_skripsi,1,NULL)) AS total_skripsi, COUNT(IF(b.id_jurnal,1,NULL)) AS total_jurnal, a.tag FROM tab_tag a JOIN tab_tag_detail b on a.id=b.id  where b.status = '1'  group by a.tag");
        $data =array(
            'thisContent'   => 'data.statistik',
            'thisJs'        => 'Js.Jstag',
            'page1'         => 'Adminitrasi 1',
            'page2'         => 'Administrasi 2',
        );
        return view('layouts.ms', compact('d','a'))->with($data);
    }
}

