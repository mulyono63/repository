<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Welcome;

class controllerwelcome extends Controller
{
    public function home(){
        $d= \App\Welcome::all();
      
        // dd($output);
        $data = array(
            'thisContent' 	=> 'tes',
            'thisJs' 	    => 'Js.Jswelcome',
			'page1' 		=> 'Administrasi 1',
            'page2' 		=> 'Administrasi 2',
        );
        return view('layouts.ms',compact('d'))->with($data);
    }

    public function add(request $request)
    {
        $ver=new \App\Welcome;
        $ver->satu= $request->satu;
        $ver->dua= $request->dua;
        $ver->tiga= $request->tiga;
        $ver->save();
        Session::put('s', 'My message');
			
        return redirect('/');
    }
    public function alert()
    {
       
        Session::alert('s', 'My message');
			
        return redirect('/');
    }
}
