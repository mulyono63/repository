<?php

namespace App\Http\Controllers;
use Auth;
use App\User;
use App\Mhs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login()
    {
        return view('auths.login');
    }

    public function lupa()
    {
        return view('auths.lupa');
    }
    public function pospass(request $request)
    {
        // dd($request->email);
        if(DB::select("select * from users where email='$request->email'"))
        {
            
            $a="Repository123";
            $b= DB::table('users')->where('email',$request->email)->update([
                'password' => Hash::make($a),
            ]);
            Mail::send('lupapassword', compact('a'), function ($message) use ($request)
            {
                $message->subject('assalamualaikum');
                $message->from('no-replay@gmail.com','REPOSITORY');
                $message->to($request->email);
            });
    
        return redirect()->back()->with('a','Silahkan Periksa Email anda');
    }else{
        return redirect()->back()->with('a','Email Belum Terdaftar');

        }
        
    }
    public function postlogin(Request $Request){
        $a=DB::select("select * from users where email='$Request->email' and activ='1'");
        if($a)
        {
            if(Auth::attempt($Request->only('email','password'))){
                return redirect('/search_in');
            }else{
                return redirect('/login')->with('a','Password Yang Dimasukan Salah');
            }
        }else{
            return redirect('/login')->with('b','Account Belum Terdaftar atau Di Non Aktifkan');
        }
        
    }
    public function pospass_update(request $request)
        {
             $this->validate($request, [
                    'password' => 'required',
                    'confirmation' => 'required|same:password',
                    ]);

            if(!empty(DB::select("select * from users where email='$request->email'")))
            {
                $data = \App\User::find($request->email);
                $b= DB::table('users')->where('email',$request->email)->update([
                'password' => Hash::make($request->password),
            ]);
            return redirect('profile')->with('success','Password Berhasil Di Ubah');
            }else{
            return redirect('profile')->with('gagal','Gagal Merubah Password');
            }
        }


    public function logout(){
        Auth::logout();
        return redirect('/');
    }

    public function daftar()
    {
        return view('auths.daftar');
    }
 
    protected function create(Request $request)
    {
    
        $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'confirmation' => 'required|same:password',
        ]);

        $e=$request->email;
        $tam =DB::select("SELECT * from users where email='$e'");
        // dd($tam);
        if($tam)
        {
          return redirect('/daftar')->with(['success' => 'Email Sudah Di Gunakan']);
        }else{
        $pass="$request->password";
        // dd($pass);
        $uppercase = preg_match('@[A-Z]@', $pass);
        $lowercase = preg_match('@[a-z]@', $pass);
        $number    = preg_match('@[0-9]@', $pass);

        if(!$uppercase || !$lowercase || !$number || strlen($pass)<=5 or strlen($pass)>=16){
            return redirect('/daftar')->with(['pass' => 'password minimal 6 karakter maksimal 15, mengandung huruf BESAR, huruf kecil dan angka']);
        }else{
            User::create([
                'id' => $request['nim'],
                'npm' => $request['nim'],
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'role' => 'mahasiswa',
                'foto' => 'user.png',
            ]);
            
            Mhs::create([
                'npm' => $request['nim'],
                'nama' => $request['name'],
                'agama' => $request['agama'],
                'jk' => $request['jk'],
            ]);
            
            Auth::attempt($request->only('email','password'));
            return redirect('/repositorypen')->with(['suc'=>'Berhasil Mendaftar']);
            }  
        }
    }

}