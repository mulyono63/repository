<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class FrontendController extends Controller
{
    public function index()
    {
        return view('frontend.index');
    }

    //search out
    public function search(request $request)
    {
            $search = strtolower($request->get('keyword'));
            $a=4;
            if(!empty(DB::select("select * from tab_penelitian where (lower(judul) like '$search%' or lower(pembimbing_1) like '$search%' or lower(pembimbing_2) like '$search%' or year(created_at) like '$search%') and active_p=1 ")) ){

                    $name= DB::select("select a.id, a.pembimbing_1 as pem_1, a.pembimbing_2 as pem_2, a.id_user, a.judul, a.abstract, a.pengarang_1, a.pengarang_2, a.pengarang_3, a.halaman, a.file, a.active_p, a.created_at, a.updated_at,
                        (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_1) as pembimbing_1,
                        (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_2) as pembimbing_2 from tab_penelitian a where (lower(a.judul) like '$search%%' or lower(a.pembimbing_1) like '$search%%' or year(a.created_at) like '$search%%') and a.active_p=1")  ;
                    return view('frontend.repositoryhasout',compact('name','a'));

            }elseif (!empty(DB::select("select * from tab_skripsi where (lower(judul) like '$search%' or lower(pembimbing_1) like '$search%' or lower(pembimbing_2) like '$search%' or  year(created_at) like '$search%') and active_s=1")) ) {
                
                $name= DB::select("select a.id, a.id_tag, a.pembimbing_1 as pem_1, a.pembimbing_2 as pem_2, a.id_user, a.judul, a.abstract, a.pengarang_1, a.halaman, a.file, a.active_s, a.created_at, a.updated_at,
                    (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_1) as pembimbing_1,
                    (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_2) as pembimbing_2
                    from tab_skripsi a where (lower(a.judul) like '$search%' or lower(a.pembimbing_1) like '$search%' or lower(pembimbing_2) like '$search%' or year(a.created_at) like '$search%') and a.active_s=1 ")  ;
                return view('frontend.repositoryhasout',compact('name','a'));

            }else{
             
            $name= DB::select("select a.id, a.id_tag, a.pembimbing_1 as pem_1, a.pembimbing_2 as pem_2, a.id_user, a.judul, a.abstract, a.pengarang_1, a.pengarang_2, a.pengarang_3, a.halaman, a.file, a.active_j, a.created_at, a.updated_at,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_1) as pembimbing_1,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_2) as pembimbing_2 from tab_jurnal a where (lower(a.judul) like '$search%' or lower(a.pembimbing_1) like '$search%' or lower(pembimbing_2) like '$search%' or year(a.created_at) like '$search%') and a.active_j=1 ")  ; 
            // dd($name);
            return view('frontend.repositoryhasout',compact('name','a'));       
            }
    }
    public function searchp(request $request)
    {
            $search = strtolower($request->get('keyword'));
            $name= DB::select("select a.id, a.id_user, a.pembimbing_1 as pem_1, a.pembimbing_2 as pem_2, a.judul, a.abstract, a.pengarang_1, a.pengarang_2, a.pengarang_3, a.halaman, a.file, a.active_p, a.created_at, a.updated_at,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_1) as pembimbing_1,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_2) as pembimbing_2 from tab_penelitian a where (lower(a.judul) like '$search%%' or lower(a.pembimbing_1) like '$search%%' or year(a.created_at) like '$search%%') and a.active_p=1 ")  ;
            // dd($name);
            $a=1;
            return view('frontend.repositoryhasout',compact('name','a'));       
    }
    public function searchs(request $request)
    {
            $search = strtolower($request->get('keyword'));
            $name= DB::select("select a.id, a.id_tag, a.pembimbing_1 as pem_1, a.pembimbing_2 as pem_2, a.id_user, a.judul, a.abstract, a.pengarang_1, a.halaman, a.file, a.active_s, a.created_at, a.updated_at,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_1) as pembimbing_1,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_2) as pembimbing_2
                from tab_skripsi a where (lower(a.judul) like '$search%' or lower(a.pembimbing_1) like '$search%' or lower(pembimbing_2) like '$search%' or year(a.created_at) like '$search%') and a.active_s=1")  ; 
            // dd($name);
            $a=2;
            return view('frontend.repositoryhasout',compact('name','a'));       
    }
    public function searchj(request $request)
    {
            $search = strtolower($request->get('keyword'));
            $name= DB::select("select a.id, a.id_tag, a.pembimbing_1 as pem_1, a.pembimbing_2 as pem_2, a.id_user, a.judul, a.abstract, a.pengarang_1, a.pengarang_2, a.pengarang_3, a.halaman, a.file, a.active_j, a.created_at, a.updated_at,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_1) as pembimbing_1,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_2) as pembimbing_2 from tab_jurnal a where (lower(a.judul) like '$search%' or lower(a.pembimbing_1) like '$search%' or lower(pembimbing_2) like '$search%' or year(a.created_at) like '$search%') and a.active_j=1")  ;   
            // dd($name);
            $a=3;
            return view('frontend.repositoryhasout',compact('name','a'));       
    }

    //search login
    public function search_in()
    {
        return view('frontend.search_in');
    }
    public function searchin(request $request)
    {
            $search = strtolower($request->get('keyword'));
            $a=4;
            if(!empty(DB::select("select * from tab_penelitian where (lower(judul) like '$search%' or lower(pembimbing_1) like '$search%' or lower(pembimbing_2) like '$search%' or year(created_at) like '$search%') and active_p=1 ")) ){

                    $name= DB::select("select a.id, a.id_user, a.pembimbing_1 as pem_1, a.pembimbing_2 as pem_2, a.judul, a.abstract, a.pengarang_1, a.pengarang_2, a.pengarang_3, a.halaman, a.file, a.active_p, a.created_at, a.updated_at,
                        (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_1) as pembimbing_1,
                        (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_2) as pembimbing_2 from tab_penelitian a where (lower(a.judul) like '$search%%' or lower(a.pembimbing_1) like '$search%%' or year(a.created_at) like '$search%%') and a.active_p=1")  ;
                    return view('frontend.repositoryhas',compact('name','a'));

            }elseif (!empty(DB::select("select * from tab_skripsi where (lower(judul) like '$search%' or lower(pembimbing_1) like '$search%' or lower(pembimbing_2) like '$search%' or  year(created_at) like '$search%') and active_s=1")) ) {
                
                $name= DB::select("select a.id, a.id_tag, a.pembimbing_1 as pem_1, a.pembimbing_2 as pem_2, a.id_user, a.judul, a.abstract, a.pengarang_1, a.halaman, a.file, a.active_s, a.created_at, a.updated_at,
                    (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_1) as pembimbing_1,
                    (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_2) as pembimbing_2
                    from tab_skripsi a where (lower(a.judul) like '$search%' or lower(a.pembimbing_1) like '$search%' or lower(pembimbing_2) like '$search%' or year(a.created_at) like '$search%') and a.active_s=1 ")  ;
                return view('frontend.repositoryhas',compact('name','a'));

             }else{
             
            $name= DB::select("select a.id, a.id_tag, a.pembimbing_1 as pem_1, a.pembimbing_2 as pem_2, a.id_user, a.judul, a.abstract, a.pengarang_1, a.pengarang_2, a.pengarang_3, a.halaman, a.file, a.active_j, a.created_at, a.updated_at,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_1) as pembimbing_1,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_2) as pembimbing_2 from tab_jurnal a where (lower(a.judul) like '$search%' or lower(a.pembimbing_1) like '$search%' or lower(pembimbing_2) like '$search%' or year(a.created_at) like '$search%') and a.active_j=1 ")  ;  
            // dd($name);
            return view('frontend.repositoryhas',compact('name','a'));       
            }
    }
    public function searchpen(request $request)
    {
            $search = strtolower($request->get('keyword'));
            $name= DB::select("select a.id, a.id_user, a.pembimbing_1 as pem_1, a.pembimbing_2 as pem_2, a.judul, a.abstract, a.pengarang_1, a.pengarang_2, a.pengarang_3, a.halaman, a.file, a.active_p, a.created_at, a.updated_at,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_1) as pembimbing_1,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_2) as pembimbing_2 from tab_penelitian a where (lower(a.judul) like '$search%%' or lower(a.pembimbing_1) like '$search%%' or year(a.created_at) like '$search%%') and a.active_p=1")  ;
            // dd($name);
            $a=1;
            return view('frontend.repositoryhas',compact('name','a'));       
    }
    public function searchskr(request $request)
    {
            $search = strtolower($request->get('keyword'));
            $name= DB::select("select a.id, a.id_tag, a.pembimbing_1 as pem_1, a.pembimbing_2 as pem_2, a.id_user, a.judul, a.abstract, a.pengarang_1, a.halaman, a.file, a.active_s, a.created_at, a.updated_at,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_1) as pembimbing_1,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_2) as pembimbing_2
                from tab_skripsi a where (lower(a.judul) like '$search%' or lower(a.pembimbing_1) like '$search%' or lower(pembimbing_2) like '$search%' or year(a.created_at) like '$search%') and a.active_s=1")  ; 
            // dd($name);
            $a=2;
            return view('frontend.repositoryhas',compact('name','a'));       
    }
    public function searchjur(request $request)
    {
           $search = strtolower($request->get('keyword'));
            $name= DB::select("select a.id, a.id_tag, a.pembimbing_1 as pem_1, a.pembimbing_2 as pem_2, a.id_user, a.judul, a.abstract, a.pengarang_1, a.pengarang_2, a.pengarang_3, a.halaman, a.file, a.active_j, a.created_at, a.updated_at,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_1) as pembimbing_1,
                (SELECT nama FROM tab_dosen WHERE nip = a.pembimbing_2) as pembimbing_2 from tab_jurnal a where (lower(a.judul) like '$search%' or lower(a.pembimbing_1) like '$search%' or lower(pembimbing_2) like '$search%' or year(a.created_at) like '$search%') and a.active_j=1")  ;   
            // dd($name);
            $a=3;
            return view('frontend.repositoryhas',compact('name','a'));       
    }

    public function searchdata($id)
    {
        $a=Crypt::decrypt($id);
        if(DB::select("select * from tab_penelitian where id=$a"))
        {
            $c=\App\Penelitian::find($a);
            $b=$c->id;
            // dd($b);
            $ver=new \App\View;
            $ver->id_penelitian= $b;
            $ver->save();
            $name= \App\Penelitian::find($a); 
            $d= DB::select("select a.*, a.created_at, a.updated_at, a.pembimbing_1, a.pembimbing_2,
                            (select nama FROM tab_dosen  WHERE nip = a.pembimbing_1) as pem1,
                            (select nama FROM tab_dosen  WHERE nip = a.pembimbing_2) as pem2,
                            (select COUNT(tab_download.id_penelitian) from tab_download WHERE tab_download.id_penelitian=a.id ) as download, 
                            (select COUNT(tab_view.id_penelitian) from tab_view WHERE tab_view.id_penelitian=a.id) as view,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik
                            from tab_penelitian a where a.id='$a'");
            $c=DB::select("SELECT a.tag FROM tab_tag a JOIN tab_tag_detail b on a.id=b.id WHERE b.id_penelitian = '$a' ");
            // dd($d);
            return view('frontend.repositorydata',compact('name','d','c')); 
        }elseif(DB::select("select * from tab_skripsi where id=$a"))
        {
            $c=\App\Skripsi::find($a);
            $b=$c->id;
            // dd($b);
            $ver=new \App\View;
            $ver->id_skripsi= $b;
            $ver->save();
            $name= \App\Skripsi::find($a); 
            $d= DB::select("select a.*, b.tag, a.created_at, a.updated_at, a.pembimbing_1, a.pembimbing_2,
                            (select nama FROM tab_dosen  WHERE nip = a.pembimbing_1) as pem1,
                            (select nama FROM tab_dosen  WHERE nip = a.pembimbing_2) as pem2, 
                            (select COUNT(tab_download.id_skripsi) from tab_download WHERE tab_download.id_skripsi=a.id ) as download, 
                            (select COUNT(tab_view.id_skripsi) from tab_view WHERE tab_view.id_skripsi=a.id) as view,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik
                            from tab_skripsi a join tab_tag b on a.id_tag=b.id where a.id = '$a'"); 
            $c=DB::select("SELECT a.tag FROM tab_tag a JOIN tab_tag_detail b on a.id=b.id WHERE b.id_skripsi = '$a' ");  
            // dd($d);
            return view('frontend.repositorydata',compact('name','d','c'));
        }elseif(DB::select("select * from tab_jurnal where id=$a"))
        {
            $c=\App\Jurnal::find($a);
            $b=$c->id;
            // dd($b);
            $ver=new \App\View;
            $ver->id_jurnal= $b;
            $ver->save();
            $name= \App\Jurnal::find($a); 
            $d= DB::select("select a.*, b.tag, a.created_at, a.updated_at, a.pembimbing_1, a.pembimbing_2,
                            (select nama FROM tab_dosen  WHERE nip = a.pembimbing_1) as pem1,
                            (select nama FROM tab_dosen  WHERE nip = a.pembimbing_2) as pem2,
                            (select COUNT(tab_download.id_jurnal) from tab_download WHERE tab_download.id_jurnal=a.id ) as download, 
                            (select COUNT(tab_view.id_jurnal) from tab_view WHERE tab_view.id_jurnal=a.id) as view,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik
                            from tab_jurnal a join tab_tag b on a.id_tag=b.id where a.id='$a'");
            $c=DB::select("SELECT a.tag FROM tab_tag a JOIN tab_tag_detail b on a.id=b.id WHERE b.id_jurnal = '$a' "); 
            // dd($d);
            return view('frontend.repositorydata',compact('name','d','c'));
        }      
    }

    public function searchdataout($id)
    {
        $a=Crypt::decrypt($id);
        if(DB::select("select * from tab_penelitian where id=$a"))
        {
            $c=\App\Penelitian::find($a);
            $b=$c->id;
            // dd($b);
            $ver=new \App\View;
            $ver->id_penelitian= $b;
            $ver->save();
            $name= \App\Penelitian::find($a); 
            $d= DB::select("select a.*, a.created_at, a.updated_at, a.pembimbing_1, a.pembimbing_2, 
                            (select nama FROM tab_dosen  WHERE nip = a.pembimbing_1) as pem1,
                            (select nama FROM tab_dosen  WHERE nip = a.pembimbing_2) as pem2,
                            (select COUNT(tab_download.id_penelitian) from tab_download WHERE tab_download.id_penelitian=a.id ) as download, 
                            (select COUNT(tab_view.id_penelitian) from tab_view WHERE tab_view.id_penelitian=a.id) as view,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik
                            from tab_penelitian a where a.id='$a'");
            $c=DB::select("SELECT a.tag FROM tab_tag a JOIN tab_tag_detail b on a.id=b.id WHERE b.id_penelitian = '$a' ");
            // dd($c);
            return view('frontend.repositorydataout',compact('name','d','c')); 
        }elseif(DB::select("select * from tab_skripsi where id=$a"))
        {
            $c=\App\Skripsi::find($a);
            $b=$c->id;
            // dd($b);
            $ver=new \App\View;
            $ver->id_skripsi= $b;
            $ver->save();
            $name= \App\Skripsi::find($a); 
            $d= DB::select("select a.*, b.tag, a.created_at, a.updated_at, a.pembimbing_1, a.pembimbing_2,
                            (select nama FROM tab_dosen  WHERE nip = a.pembimbing_1) as pem1,
                            (select nama FROM tab_dosen  WHERE nip = a.pembimbing_2) as pem2, 
                            (select COUNT(tab_download.id_skripsi) from tab_download WHERE tab_download.id_skripsi=a.id ) as download, 
                            (select COUNT(tab_view.id_skripsi) from tab_view WHERE tab_view.id_skripsi=a.id) as view,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik
                            from tab_skripsi a join tab_tag b on a.id_tag=b.id where a.id = '$a'"); 
            $c=DB::select("SELECT a.tag FROM tab_tag a JOIN tab_tag_detail b on a.id=b.id WHERE b.id_skripsi = '$a' "); 
            // dd($d);
            return view('frontend.repositorydataout',compact('name','d','c'));
        }elseif(DB::select("select * from tab_jurnal where id=$a"))
        {
            $c=\App\Jurnal::find($a);
            $b=$c->id;
            // dd($b);
            $ver=new \App\View;
            $ver->id_jurnal= $b;
            $ver->save();
            $name= \App\Jurnal::find($a); 
            $d= DB::select("select a.*, b.tag, a.created_at, a.updated_at, a.pembimbing_1, a.pembimbing_2,
                            (select nama FROM tab_dosen  WHERE nip = a.pembimbing_1) as pem1,
                            (select nama FROM tab_dosen  WHERE nip = a.pembimbing_2) as pem2,
                            (select COUNT(tab_download.id_jurnal) from tab_download WHERE tab_download.id_jurnal=a.id ) as download, 
                            (select COUNT(tab_view.id_jurnal) from tab_view WHERE tab_view.id_jurnal=a.id) as view,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik
                            from tab_jurnal a join tab_tag b on a.id_tag=b.id where a.id='$a'");
            $c=DB::select("SELECT a.tag FROM tab_tag a JOIN tab_tag_detail b on a.id=b.id WHERE b.id_jurnal = '$a' "); 
            // dd($d);
            return view('frontend.repositorydataout',compact('name','d','c'));
        }      
    }

    public function seearchrepositorypen()
    {
        return view('frontend.repositorypen');
    }
    public function seearchrepositoryskr()
    {
        return view('frontend.repositoryskr');
    }
    public function seearchrepositoryjur()
    {
        return view('frontend.repositoryjur');
    }

    //search frond
    public function seearchrepositoryp()
    {
        return view('frontend.repositoryp');
    }
    public function seearchrepositorys()
    {
        return view('frontend.repositorys');
    }
    public function seearchrepositoryj()
    {
        return view('frontend.repositoryj');
    }

    public function dosen($pembimbing)
    {
        // dd($pembimbing);
            // $name= DB::select("select * from tab_dosen where judul like '%$search%' and active_j=1 ")  ; 
            // return view('frontend.repositoryhas',compact('name');       
    }

    public function profile_dosen($pembimbing){
        $d= DB::select("select * from tab_dosen where nip='$pembimbing'");
        $f= DB::select("select * from users where npm='$pembimbing'");
        $b= DB::select("select * from tab_pendidikan where id_dosen='$pembimbing'");
        $e=DB::select("select a.*, 
                            (select COUNT(tab_download.id_penelitian) from tab_download WHERE tab_download.id_penelitian=a.id ) as download_penelitian, 
                            (select COUNT(tab_view.id_penelitian) from tab_view WHERE tab_view.id_penelitian=a.id) as view_penelitian,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik_penelitian
                            from tab_penelitian a where id_user='$pembimbing'");
        $c= DB::select("select * from tab_documen where id_dosen='$pembimbing'");
        $g=DB::select("SELECT (SELECT COUNT(tab_skripsi.id) from tab_skripsi WHERE tab_skripsi.pembimbing_1 = a.nip or tab_skripsi.pembimbing_2 = a.nip) as total_skripsi,
                (SELECT COUNT(tab_jurnal.id) from tab_jurnal WHERE tab_jurnal.pembimbing_1 = a.nip or tab_jurnal.pembimbing_2 = a.nip) as total_jurnal,
                (SELECT COUNT(tab_penelitian.id) from tab_penelitian WHERE tab_penelitian.pembimbing_1 = a.nip or tab_penelitian.pembimbing_2 = a.nip) as total_penelitian
                FROM tab_dosen a WHERE a.nip = '$pembimbing'");
        return view('data.profile_dosen', compact('b','c','d','e','f','g'));
    }
    public function profile_dosenin($pembimbing){
        $d= DB::select("select * from tab_dosen where nip='$pembimbing'");
        $f= DB::select("select * from users where npm='$pembimbing'");
        $b= DB::select("select * from tab_pendidikan where id_dosen='$pembimbing'");
        $e=DB::select("select a.*, 
                            (select COUNT(tab_download.id_penelitian) from tab_download WHERE tab_download.id_penelitian=a.id ) as download_penelitian, 
                            (select COUNT(tab_view.id_penelitian) from tab_view WHERE tab_view.id_penelitian=a.id) as view_penelitian,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik_penelitian
                            from tab_penelitian a where id_user='$pembimbing'");
        $c= DB::select("select * from tab_documen where id_dosen='$pembimbing'");
        $g=DB::select("SELECT (SELECT COUNT(tab_skripsi.id) from tab_skripsi WHERE tab_skripsi.pembimbing_1 = a.nip or tab_skripsi.pembimbing_2 = a.nip) as total_skripsi,
                (SELECT COUNT(tab_jurnal.id) from tab_jurnal WHERE tab_jurnal.pembimbing_1 = a.nip or tab_jurnal.pembimbing_2 = a.nip) as total_jurnal,
                (SELECT COUNT(tab_penelitian.id) from tab_penelitian WHERE tab_penelitian.pembimbing_1 = a.nip or tab_penelitian.pembimbing_2 = a.nip) as total_penelitian
                FROM tab_dosen a WHERE a.nip = '$pembimbing'");
        return view('data.profile_dosenin', compact('b','c','d','e','f','g'));
    }

    public function dosen_list(){
        $d= DB::select("select a.*, b.foto from tab_dosen a join users b on a.nip = b.npm ");
        return view('data.list_dosen', compact('d'));
    }

    public function dosen_listin(){
        $d= DB::select("select a.*, b.foto from tab_dosen a join users b on a.nip = b.npm ");
        return view('data.list_dosenin', compact('d'));
    }
}
