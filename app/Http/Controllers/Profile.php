<?php

namespace App\Http\Controllers;

use App\Dosen;
use App\Documen;
use App\Pendidikan;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;

class Profile extends Controller
{
    public function profile()
    {
        $a=Auth::user()->npm;
        if(DB::select("select * from tab_dosen where nip='$a'"))
        {
            $d= DB::select("select * from tab_dosen where nip='$a'");
            $b= DB::select("select * from tab_pendidikan where id_dosen='$a'");
            $e=DB::select("select a.*, 
                            (select COUNT(tab_download.id_penelitian) from tab_download WHERE tab_download.id_penelitian=a.id ) as download_penelitian, 
                            (select COUNT(tab_view.id_penelitian) from tab_view WHERE tab_view.id_penelitian=a.id) as view_penelitian,
                            (select COUNT(tab_petik.id) from tab_petik WHERE tab_petik.id=a.id) as petik_penelitian
                            from tab_penelitian a where id_user='$a'");
            $c= DB::select("select * from tab_documen where id_dosen='$a'");
            $f=DB::select("SELECT (SELECT COUNT(tab_skripsi.id) from tab_skripsi WHERE tab_skripsi.pembimbing_1 = a.nip or tab_skripsi.pembimbing_2 = a.nip) as total_skripsi,
                (SELECT COUNT(tab_jurnal.id) from tab_jurnal WHERE tab_jurnal.pembimbing_1 = a.nip or tab_jurnal.pembimbing_2 = a.nip) as total_jurnal,
                (SELECT COUNT(tab_penelitian.id) from tab_penelitian WHERE tab_penelitian.pembimbing_1 = a.nip or tab_penelitian.pembimbing_2 = a.nip) as total_penelitian
                FROM tab_dosen a WHERE a.nip = '$a'");
            // dd($a);
            $data = array(
                'thisContent'   => 'profile.profile',
                'thisJs'        => 'Js.Jsprofile',
                'page1'         => 'Administrasi 1',
                'page2'         => 'Administrasi 2',
            );
            // dd($d[0]->nama);
            return view('layouts.ms',compact('d', 'b', 'c', 'e', 'f'))->with($data);
        }elseif(DB::select("select * from tab_mhs where npm='$a'"))
        {
            $d= DB::select("select id, npm as a, nama,gelar, pendidikan, agama, jk, tempat_lahir, tgl_lahir, created_at, updated_at from tab_mhs where npm='$a'");
            // dd($d);
            $data = array(
                'thisContent'   => 'profile.profile',
                'thisJs'        => 'Js.Jswelcome',
                'page1'         => 'Administrasi 1',
                'page2'         => 'Administrasi 2',
            );
            return view('layouts.ms',compact('d'))->with($data);
        }else{
            $d= DB::select("select nip as a, nama, gelar, pendidikan, created_at, updated_at, nidn, jabatan, tempat_lahir, tgl_lahir, jk from tab_dosen where nip");
            // dd($d);
            $data = array(
                'thisContent'   => 'profile.profile',
                'thisJs'        => 'Js.Jswelcome',
                'page1'         => 'Administrasi 1',
                'page2'         => 'Administrasi 2',
            );
            return view('layouts.ms',compact('d'))->with($data);
        }
        
    }

    public function documen()
    {
            $a=Auth::user()->npm;
            $c= DB::select("select * from tab_documen where id_dosen='$a'");
            // dd($a);
            $data = array(
                'thisContent'   => 'profile.documen',
                'thisJs'        => 'Js.Jsprofile',
                'page1'         => 'Administrasi 1',
                'page2'         => 'Administrasi 2',
            );
            // dd($d[0]->nama);
            return view('layouts.ms',compact('c'))->with($data);
    }

    public function posprofil(request $request)
    {
        if($request->foto_del == "user.png")
        {
            $this->validate($request, [
                'foto' => 'required|mimes:jpg,png,jpeg|max:2048',
            ]);

            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('foto');
        
            $nama_file = time()."_".$file->getClientOriginalName();
     
              // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'img/userimg';
            $file->move($tujuan_upload,$nama_file);
    
            $a= DB::table('users')->where('id',$request->id)->update([
                'foto' => $nama_file,
            ]);
            return redirect()->back();
        }else{

            File::delete('img/userimg/'.$request->foto_del);
                
            $this->validate($request, [
                'foto' => 'required|mimes:jpg,png,jpeg|max:2048',
            ]);
    
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('foto');
        
            $nama_file = time()."_".$file->getClientOriginalName();
     
              // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'img/userimg';
            $file->move($tujuan_upload,$nama_file);
    
            $a= DB::table('users')->where('id',$request->id)->update([
                'foto' => $nama_file,
            ]);
            return redirect()->back();
        }
    }

    public function posep(request $request)
    {
        $a=Auth::user()->npm;
        if(DB::select("select * from tab_mhs where npm='$a'"))
        {
            $a= DB::table('users')->where('id',$a)->update([
                'name' => $request['username'],
            ]);
            $a= DB::table('tab_mhs')->where('id',$request->id)->update([
                'nama' => $request['name'],
                'gelar' => $request['gelar'],
                'pendidikan' => $request['pendidikan'],
                'agama' => $request['agama'],
                'jk' => $request['jk'],
                'tempat_lahir' => $request['tempat_lahir'],
                'tgl_lahir' => $request['tgl_lahir'],
            ]);
            return redirect()->back();
        }else{
            return redirect()->back();
        }
        
    }

    public function pospass(request $request)
    {
        
        $this->validate($request, [
            'password' => 'required',
            'password_conf' => 'required|same:password',
        ]);
            $a=Auth::user()->id;
            $b= DB::table('users')->where('id',$a)->update([
                'password' => Hash::make($request['password']),
            ]);
            return redirect()->back();
    }

   public function posdocumen(request $request)
   {
     $file = $request->file('nama_file');
        
            $nama_file = time()."_".$file->getClientOriginalName();
     
              // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'documen';
            $file->move($tujuan_upload,$nama_file);
            
            Documen::create([
                'id_dosen' => Auth::user()->npm,
                'judul_file' => $request['judul_file'],
                'nama_file' => $nama_file,
            ]);
    
            return redirect()->back();
   }

    public function deletedocumen($id, $file)
    {
        // dd($file);
        File::delete("documen/".$file);
        $data = \App\Documen::find($id);
        $data->delete($data);
        return redirect()->back();
    }

    public function pospendidikan(request $request)
   {
    if($request->flag_edit==1)
    {
        $a= DB::table('tab_pendidikan')->where('id',$request->id)->update([
                'perguruan_tinggi' => $request['perguruan_tinggi'],
                'gelar' => $request['gelar'],
                'tahun_ijasah' => $request['tahun_ijasah'],
                'jenjang' => $request['jenjang'],
            ]);
            return redirect()->back();
    }else{
        Pendidikan::create([
                'id_dosen' => Auth::user()->npm,
                'perguruan_tinggi' => $request['perguruan_tinggi'],
                'gelar' => $request['gelar'],
                'tahun_ijasah' => $request['tahun_ijasah'],
                'jenjang' => $request['jenjang'],
            ]);
    
            return redirect()->back();
    }
   }

    public function deletependidikan($id)
    {
        // dd($file);
        $data = \App\Pendidikan::find($id);
        $data->delete($data);
        return redirect()->back();
    }

    public function posbiodata(request $request)
    {
        $a= DB::table('tab_dosen')->where('id',$request->id)->update([
                'nama' => $request['nama'],
                'gelar' => $request['gelar'],
                'pendidikan' => $request['pendidikan'],
                'agama' => $request['agama'],
                'jk' => $request['jk'],
                'nidn' => $request['nidn'],
                'jabatan' => $request['jabatan'],
                'tempat_lahir' => $request['tempat_lahir'],
                'tgl_lahir' => $request['tgl_lahir'],
            ]);
            return redirect()->back();
    }

    
}
