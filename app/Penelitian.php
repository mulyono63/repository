<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penelitian extends Model
{
    protected $table="tab_penelitian";
    protected $fillable=['id','id_user','pembimbing_1','pembimbing_2','judul','abstract', 'pengarang_1','pengarang_2','pengarang_3','halaman','file','active_p'];
}
