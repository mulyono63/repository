<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    protected $table="tab_view";
    protected $fillable=['id','id_penelitian','id_skripsi', 'id_jurnal'];
}
