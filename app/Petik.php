<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Petik extends Model
{
    protected $table="tab_petik";
    protected $fillable=['id','id_penelitian','id_skripsi', 'id_jurnal'];
}
