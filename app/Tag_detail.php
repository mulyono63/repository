<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag_detail extends Model
{
    protected $table="tab_tag_detail";
    protected $fillable=['id','id_penelitian','id_skripsi', 'id_jurnal'];
}
