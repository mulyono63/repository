<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mhs extends Model
{
    protected $table="tab_mhs";
    protected $fillable=['id','npm','nama','gelar','pendidikan','agama','jk','tempat_lahir', 'tgl_lahir'];
}
