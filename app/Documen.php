<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documen extends Model
{
    protected $table="tab_documen";
    protected $fillable=['id','id_dosen','judul_file', 'nama_file'];
}
