<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table="tab_tag";
    protected $fillable=['id','tag'];
}
